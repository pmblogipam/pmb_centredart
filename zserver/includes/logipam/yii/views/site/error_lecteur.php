<div class='alert alert-danger'>
    <h3><i class='fa fa-warning'></i> Le code "<strong><?= $code ?></strong>" n'est pas un code valide de lecteur !</h3>
</div>