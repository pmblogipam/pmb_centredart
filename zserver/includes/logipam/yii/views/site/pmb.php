<div> 
<form id='notice'>
  
     <div class="row">
         <div class="col-xs-4">
             <div class="form-group">
                <label for="no-inventaire">Num&eacute;ro Inventaire</label>
                <input type="text" class="form-control" id="no-inventaire">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                <label for="ancien-no-inventaire">Ancien Num&eacute;ro Inventaire</label>
                <input type="text" class="form-control" id="ancien-no-inventaire">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                <label for="artiste">Artiste</label>
                <div class='input-group'>
                   
                <input completion="authors" autfield="f_aut0_id" id="auteur0" type="text" readonly="readonly" class="form-control" name="f_aut0" data-form-name="f_aut0" value=""  onclick="popupCenter('./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&deb_rech=', 'myPop1',450,450);">
               
                <!--
                <input completion="authors" autfield="f_aut0_id" id="auteur0" class="saisie-30emr uk-input uk-form-width-medium wyr-input" name="f_aut0" data-form-name="f_aut0" value="" autocomplete="off" type="text">
                -->
                <span class="input-group-btn" id='artiste_select'> <button type="button" class="btn btn-warning" onclick="openPopUp('./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&deb_rech='+encodeURIComponent(this.form.f_aut0.value), 'select_author0', 500, 400, -2, -2, 'scrollbars=yes, toolbar=no, dependent=yes, resizable=yes')">...
                 </button> </span>
                <input type='hidden' name='f_aut0_id' data-form-name='f_aut0_id' id='f_aut0_id' value="0" />
                </div>
             </div>
         </div>
     </div>     
     
     <div class="row">
         <div class="col-xs-4">
             <div class="form-group">
                <label for="titre">Titre</label>
                 <input type="text" class="form-control" id="titre">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="titre-provisoire">Titre provisoire</label>
                <input type="text" class="form-control" id="titre-provisoire">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="date-creation">Date cr&eacute;ation</label>
                <input type="text" class="form-control" id="date-creation">
              </div>
         </div>
     </div> 
     
     <div class="row">
         <div class="col-xs-3">
             <div class="form-group">
                <label for="lieu-creation">Lieu de creation</label>
                 <input type="text" class="form-control" id="lieu-creation">
              </div>
         </div>
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="hauteur-cm">Hauteur (cm)</label>
                <input type="text" class="form-control" id="hauteur-cm">
              </div>
         </div>
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="largeur-cm">Largeur (cm)</label>
                <input type="text" class="form-control" id="largeur-cm">
              </div>
         </div>
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="profondeur-cm">Profondeur (cm)</label>
                <input type="text" class="form-control" id="profondeur-cm">
              </div>
         </div>
     </div>
     
     <div class="row">
         
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="hauteur-in">Hauteur (in)</label>
                <input type="text" class="form-control" id="hauteur-in">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="largeur-in">Largeur (in)</label>
                <input type="text" class="form-control" id="largeur-in">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="profondeur-in">Profondeur (in)</label>
                <input type="text" class="form-control" id="profondeur-in">
              </div>
         </div>
     </div>
     
     <div class="row">
         
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="materiau">Mat&eacute;riau</label>
                 <div class='input-group'>
                <input type="text" class="form-control" readonly="readonly" id="materiau">
                <span class="input-group-btn" id='artiste_select'> <button type="button" class="btn btn-warning">...
                 </button> </span>
                 </div>
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="categorie">Cat&eacute;gorie</label>
                 <div class='input-group'>
                    <input completion="authors" autfield="f_aut0_id1" id="auteur01" type="text" readonly="readonly" class="form-control" name="f_aut01" data-form-name="f_aut0" value=""  onclick="popupCenter('./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&deb_rech=', 'myPop1',450,450);">
               
                <!--
                <input completion="authors" autfield="f_aut0_id" id="auteur0" class="saisie-30emr uk-input uk-form-width-medium wyr-input" name="f_aut0" data-form-name="f_aut0" value="" autocomplete="off" type="text">
                -->
                <span class="input-group-btn" id='artiste_select'> <button type="button" class="btn btn-warning" onclick="openPopUp('./includes/logipam/yii/web/index.php?r=site/motscles&what=auteur&caller=notice&param1=f_aut0_id1&param2=f_aut01&deb_rech='+encodeURIComponent(this.form.f_aut01.value), 'select_author0', 500, 400, -2, -2, 'scrollbars=yes, toolbar=no, dependent=yes, resizable=yes')">...
                 </button> </span>
                <input type='hidden' name='f_aut0_id1' data-form-name='f_aut0_id1' id='f_aut0_id1' value="0" /> 
                     
                     <!--
                <input type="text" class="form-control" readonly="readonly" id="categorie">
                <span class="input-group-btn" id='artiste_select'> <button type="button" class="btn btn-warning" onclick="openPopUp('./includes/logipam/yii/web/index.php?r=site/motscles', 'select_author0', 500, 400, -2, -2, 'scrollbars=yes, toolbar=no, dependent=yes, resizable=yes')">...
                 </button>  </span>
                     -->
                 </div>
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="description">Description</label>
                <input type="text" class="form-control" id="description">
              </div>
         </div>
     </div>
     
     <div class="row">
         
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="exposition">Exposition</label>
                 <!--
                 <input type="text" class="form-control" id="exposition"><button class='btn btn-warning btn-sm'><i class="fa fa-plus"></i></button>
              -->
              <div class='input-group'>
              <input completion="authors" autfield="f_aut0_id2" id="auteur02" type="text" readonly="readonly" class="form-control" name="f_aut02" data-form-name="f_aut02" value=""  onclick="popupCenter('./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&deb_rech=', 'myPop1',450,450);">
               
                <!--
                <input completion="authors" autfield="f_aut0_id" id="auteur0" class="saisie-30emr uk-input uk-form-width-medium wyr-input" name="f_aut0" data-form-name="f_aut0" value="" autocomplete="off" type="text">
                -->
                <span class="input-group-btn" id='artiste_select'> <button type="button" class="btn btn-warning" onclick="openPopUp('./includes/logipam/yii/web/index.php?r=site/motscles&what=auteur&caller=notice&param1=f_aut0_id1&param2=f_aut01&deb_rech='+encodeURIComponent(this.form.f_aut01.value), 'select_author0', 500, 400, -2, -2, 'scrollbars=yes, toolbar=no, dependent=yes, resizable=yes')"><i class='fa fa-plus'></i>
                 </button> </span>
                <input type='hidden' name='f_aut0_id2' data-form-name='f_aut0_id2' id='f_aut0_id2' value="0" /> 
              </div>
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="mots-cle">Mots clés</label>
                 <!--
                <input type="text" class="form-control" id="mots-cle"><button class='btn btn-warning btn-sm'><i class="fa fa-plus"></i></button>
              -->
              <div class='input-group'>
              <input completion="authors" autfield="f_aut0_id2" id="auteur02" type="text" readonly="readonly" class="form-control" name="f_aut02" data-form-name="f_aut02" value=""  onclick="popupCenter('./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&deb_rech=', 'myPop1',450,450);">
               
                <!--
                <input completion="authors" autfield="f_aut0_id" id="auteur0" class="saisie-30emr uk-input uk-form-width-medium wyr-input" name="f_aut0" data-form-name="f_aut0" value="" autocomplete="off" type="text">
                -->
                <span class="input-group-btn" id='artiste_select'> <button type="button" class="btn btn-warning" onclick="openPopUp('./includes/logipam/yii/web/index.php?r=site/motscles&what=auteur&caller=notice&param1=f_aut0_id1&param2=f_aut01&deb_rech='+encodeURIComponent(this.form.f_aut01.value), 'select_author0', 500, 400, -2, -2, 'scrollbars=yes, toolbar=no, dependent=yes, resizable=yes')">...
                 </button> </span>
                <input type='hidden' name='f_aut0_id2' data-form-name='f_aut0_id2' id='f_aut0_id2' value="0" /> 
              </div>
             </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="statut">Statut</label>
               <!-- <input type="text" class="form-control" id="statut"> -->
                 <select class="form-control" id="statut">
                     <option value="1">Collection Permanente</option>
                     <option value="2">En consignation</option>
                 </select>
              </div>
         </div>
     </div>
     
     <div class="row">
         
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="provenance">Provenance</label>
                 <input type="text" class="form-control" id="provenance">
              </div>
         </div>
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="prix-achat">Prix d'achat</label>
                <input type="text" class="form-control" id="prix-achat">
              </div>
         </div>
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="valeur-assurance">Valeur assurance</label>
               <input type="text" class="form-control" id="valeur-assurance">
                 
              </div>
         </div>
         <div class="col-xs-3">
             <div class="form-group">
                 <label for="prix-vente">Prix de vente</label>
               <input type="text" class="form-control" id="prix-vente">
                 
              </div>
         </div>
     </div>
     
     <div class="row">
         
         <div class="col-xs-4">
             <div class="form-group">
                  <label for="localisation">Localisation</label>
               <select class="form-control" id="localisation">
                     <option value="1">Centre d'art</option>
                     <option value="2">Depot 1</option>
                 </select>
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="eta-conservation">Etat de conservation</label>
               <input type="text" class="form-control" id="etat-conservation">
              </div>
         </div>
         <div class="col-xs-4">
             <div class="form-group">
                 <label for="restauration">Restauration effectu&eacute;e</label>
               <input type="text" class="form-control" id="restauration">
                 
              </div>
         </div>
     </div>
     
     <div class='row'>
         <div class='col-xs-4'>
             
         </div>
         <div class='col-xs-2'>
             <button class='btn btn-success'>Enregistrer</button>
         </div>
         
         
         <div class='col-xs-2'>
             <button class='btn btn-info'>Annuler</button>
         </div>
         
         <div class='col-xs-4'>
             
         </div>
     </div>
     
     
</form> 
</div>


<div id='searchPlace'></div>


 
 


<script>
    
$(document).ready(function(){
    $("#searchPlace").hide(); 
    $("#addOeuvre").click(function(){
        $("#notice").show(); 
        $("#searchPlace").hide();
    });
    $('#date-creation').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        $("#search").click(function(){
            $("#notice").hide(); 
            $("#searchPlace").show(); 
            $.get('includes/logipam/yii/web/index.php?r=site/get-search',{},function(data){
            
            $('#searchPlace').html(data);
            });
        });
});    
function popupCenter(url, title, w, h) {
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function fonction_selecteur_auteur() {
        name=this.getAttribute('id').substring(4);
        name_id = name.substr(0,6)+'_id'+name.substr(6);
        openPopUp('./select.php?what=auteur&caller=notice&param1='+name_id+'&param2='+name+'&dyn=1&deb_rech='+encodeURIComponent(document.getElementById(name).value), 'select_author2', 500, 400, -2, -2, 'scrollbars=yes, toolbar=no, dependent=yes, resizable=yes');
    }

</script>

<script>
 
    
    $(document).ready(function(){
        
        
        $('.typeahead_1').typeahead({
                source: ["item 1","item 2","item 3"]
            });
            
            $('#artiste').typeahead({
                source: [
                    {"name": "Afghanistan", "code": "AF", "ccn0": "040"},
                    {"name": "Land Islands", "code": "AX", "ccn0": "050"},
                    {"name": "Albania", "code": "AL","ccn0": "060"},
                    {"name": "Algeria", "code": "DZ","ccn0": "070"}
                ]
            });
            
            $("#artiste").change(function(){
                alert("testy");
            });
            
            
    }); 
    
   
</script>

  <script>
  $( function() {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      name = $( "#name" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      valid = valid && checkLength( name, "username", 3, 16 );
      valid = valid && checkLength( email, "email", 6, 80 );
      valid = valid && checkLength( password, "password", 5, 16 );
 
      valid = valid && checkRegexp( name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
      valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
      valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
 
      if ( valid ) {
        $( "#users tbody" ).append( "<tr>" +
          "<td>" + name.val() + "</td>" +
          "<td>" + email.val() + "</td>" +
          "<td>" + password.val() + "</td>" +
        "</tr>" );
        dialog.dialog( "close" );
      }
      return valid;
    }
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Create an account": addUser,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser();
    });
 
    $( "#create-user" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });
  } );
  </script>