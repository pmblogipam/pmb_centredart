<?php
use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
  
     
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2">
            <ul class="sidebar-menu tree" data-widget="tree">
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-calendar"></i>
                    <span>Oeuvre d'art</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: block;">
                    <li id="addOeuvre"><a href="#"><i class="fa fa-plus"></i> Creation oeuvre d'art</a></li>
                    <li id="search"><a href="#"><i class="fa fa-search"></i> Rechercher</a></li>
                    <li id="pret"><a href="#"><i class="fa fa-cart-plus"></i> Prets et exposition</a></li>
                    
                  </ul>
                </li>
                
                
            </ul>
           
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10"> <?=  $content ?></div>
        
    </div>

</html>
<?php $this->endPage() ?>        
    
  
