<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empr".
 *
 * @property integer $id_empr
 * @property string $empr_cb
 * @property string $empr_nom
 * @property string $empr_prenom
 * @property string $empr_adr1
 * @property string $empr_adr2
 * @property string $empr_cp
 * @property string $empr_ville
 * @property string $empr_pays
 * @property string $empr_mail
 * @property string $empr_tel1
 * @property string $empr_tel2
 * @property string $empr_prof
 * @property integer $empr_year
 * @property integer $empr_categ
 * @property integer $empr_codestat
 * @property string $empr_creation
 * @property string $empr_modif
 * @property integer $empr_sexe
 * @property string $empr_login
 * @property string $empr_password
 * @property integer $empr_password_is_encrypted
 * @property string $empr_digest
 * @property string $empr_date_adhesion
 * @property string $empr_date_expiration
 * @property string $empr_msg
 * @property string $empr_lang
 * @property integer $empr_ldap
 * @property integer $type_abt
 * @property string $last_loan_date
 * @property integer $empr_location
 * @property string $date_fin_blocage
 * @property string $total_loans
 * @property string $empr_statut
 * @property string $cle_validation
 * @property integer $empr_sms
 * @property string $empr_subscription_action
 */
class Empr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empr_year', 'empr_categ', 'empr_codestat', 'empr_sexe', 'empr_password_is_encrypted', 'empr_ldap', 'type_abt', 'empr_location', 'total_loans', 'empr_statut', 'empr_sms'], 'integer'],
            [['empr_creation', 'empr_modif', 'empr_date_adhesion', 'empr_date_expiration', 'last_loan_date', 'date_fin_blocage'], 'safe'],
            [['empr_msg', 'empr_subscription_action'], 'string'],
            [['empr_cb', 'empr_nom', 'empr_prenom', 'empr_adr1', 'empr_adr2', 'empr_ville', 'empr_pays', 'empr_mail', 'empr_tel1', 'empr_tel2', 'empr_prof', 'empr_login', 'empr_password', 'empr_digest', 'cle_validation'], 'string', 'max' => 255],
            [['empr_cp', 'empr_lang'], 'string', 'max' => 10],
            [['empr_cb'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_empr' => 'Id Empr',
            'empr_cb' => 'Empr Cb',
            'empr_nom' => 'Empr Nom',
            'empr_prenom' => 'Empr Prenom',
            'empr_adr1' => 'Empr Adr1',
            'empr_adr2' => 'Empr Adr2',
            'empr_cp' => 'Empr Cp',
            'empr_ville' => 'Empr Ville',
            'empr_pays' => 'Empr Pays',
            'empr_mail' => 'Empr Mail',
            'empr_tel1' => 'Empr Tel1',
            'empr_tel2' => 'Empr Tel2',
            'empr_prof' => 'Empr Prof',
            'empr_year' => 'Empr Year',
            'empr_categ' => 'Empr Categ',
            'empr_codestat' => 'Empr Codestat',
            'empr_creation' => 'Empr Creation',
            'empr_modif' => 'Empr Modif',
            'empr_sexe' => 'Empr Sexe',
            'empr_login' => 'Empr Login',
            'empr_password' => 'Empr Password',
            'empr_password_is_encrypted' => 'Empr Password Is Encrypted',
            'empr_digest' => 'Empr Digest',
            'empr_date_adhesion' => 'Empr Date Adhesion',
            'empr_date_expiration' => 'Empr Date Expiration',
            'empr_msg' => 'Empr Msg',
            'empr_lang' => 'Empr Lang',
            'empr_ldap' => 'Empr Ldap',
            'type_abt' => 'Type Abt',
            'last_loan_date' => 'Last Loan Date',
            'empr_location' => 'Empr Location',
            'date_fin_blocage' => 'Date Fin Blocage',
            'total_loans' => 'Total Loans',
            'empr_statut' => 'Empr Statut',
            'cle_validation' => 'Cle Validation',
            'empr_sms' => 'Empr Sms',
            'empr_subscription_action' => 'Empr Subscription Action',
        ];
    }
    
    public function getNom($empr_code){
        return Empr::find()->where(['=','empr_cb',$empr_code])->one()->empr_nom; 
    }
    
    public function getPrenom($empr_code){
        return Empr::find()->where(['=','empr_cb',$empr_code])->one()->empr_prenom; 
    }
    
    public function getSexe($empr_code){
        return Empr::find()->where(['=','empr_cb',$empr_code])->one()->empr_sexe; 
    }
    
    public function getDateAdhesion($empr_code){
        return Empr::find()->where(['=','empr_cb',$empr_code])->one()->empr_date_adhesion; 
    }
    
    public function getDateExpiration($empr_code){
        return Empr::find()->where(['=','empr_cb',$empr_code])->one()->empr_date_expiration; 
    }
    
}
