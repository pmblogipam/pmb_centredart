<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\LogipamPresence; 
use app\models\Empr; 
use app\models\LogipamCarteMembre; 
use kartik\mpdf\Pdf; // For PDF 

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "layout_pmb";
        return $this->render('pmb');
    }
    
    public function actionMotscles()
    {
        $this->layout = "pdf_layout";
        return $this->render('motscles');
    }
    
    public function actionGetSearch(){
       return $this->renderAjax('search'); 
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    public function actionGetLecteurInfo($code){
       $count_lecteur = Empr::find()->where(['=','empr_cb',$code])->count(); 
       if($count_lecteur==0){
           echo $this->renderAjax('error_lecteur',['code'=>$code]);
       }else{
           echo $this->renderAjax('empr',['code'=>$code]);
       }
        
       
    }
    
    public function actionGetRaportParam($date1, $date2){
        echo $this->renderAjax('raport_param',['date1'=>$date1,'date2'=>$date2]);
    }
    
    
    public function actionGetRapportVisite(){
       
      echo  $this->renderAjax('raport');  
    }
    
    public function actionGetHeure(){
        echo date('Y-m-d h:i:s');
    }
    
    /**
     * 
     * 
     */
    public function actionGetTotalVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $visite_par_jour = LogipamPresence::find()->where(['=','date_presence',$today])->count();  
        echo $visite_par_jour; 
    }
    
    public function actionGetTotalFemaleVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = 2 AND lp.date_presence = '$today'";
        $visite_female_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_female_jour; 
    }
    
    public function actionGetTotalMaleVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = 1 AND lp.date_presence = '$today'";
        $visite_male_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_male_jour; 
    }
    
    public function actionGetAdhesionDepassee(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id_empr, empr_date_expiration FROM empr WHERE empr_date_expiration <= '$today'"; 
        $total_adhesion_depassee = LogipamPresence::findBySql($sql_str)->count(); 
        echo $total_adhesion_depassee; 
    }
    
    public function actionGetRapportListe(){
        echo $this->renderAjax('liste');
    }
    
    
  public function actionUpload(){
        $fileName = 'file';
        $uploadPath = '../../../../photos/lecteurs';
       
        if (isset($_FILES[$fileName])) {
            
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            
           // echo $empr_code; 
            /*
            if(isset($data_empr)){
                $empr_cb = $empr_code; 
                $id_empr = $data_empr->id_empr;
                $prenom = $data_empr->empr_prenom;
                $nom = $data_empr->empr_nom; 
                $sexe = $data_empr->empr_sexe; 
                $date_adhesion = $data_empr->empr_date_adhesion;
                $date_expiration = $data_empr->empr_date_expiration;
              */  
                
            
                if ($file->saveAs($uploadPath . '/' . $file->name)) {
                   
                   $file_path = $uploadPath . '/' . $file->name;
                    // jpg  change the dimension 750, 450 to your desired values
                   $img = $this->resize_imagejpg($file_path, 200, 200);
                    // again for jpg
                    imagejpeg($img, $uploadPath . '/' . $file->name);
                   // $img = resize_imagejpg($file_path, 200, 200);
                    $carteMembre = new LogipamCarteMembre();
                   // $data_empr = new Empr();
                    $empr_code = substr($file->name,0,-4);
                    
                   $data_empr = Empr::findBySql("SELECT empr_prenom, empr_nom, empr_sexe, empr_date_adhesion, empr_date_expiration FROM empr WHERE empr_cb = '$empr_code'")->asArray()->all();
                    //print_r($data_empr);
                   // $empr_cb = $empr_code; 
                    //$id_empr = $data_empr->id_empr;
                           
                    $prenom = $data_empr[0]['empr_prenom'];
                    $nom = $data_empr[0]['empr_nom']; 
                    $sexe = $data_empr[0]['empr_sexe']; 
                    $date_adhesion = $data_empr[0]['empr_date_adhesion'];
                    $date_expiration = $data_empr[0]['empr_date_expiration'];
                    
                    
                //Now save file data to database
                  
                    $carteMembre->empr_cb = $empr_code; 
                    $carteMembre->image_name = $file->name;//$dataempr->getPrenom($empr_code);
                    $carteMembre->date_ajout = date('Y-m-d h:m:s');
                    $carteMembre->prenom = $prenom; 
                    $carteMembre->nom = $nom; 
                    $carteMembre->sexe = $sexe; 
                    $carteMembre->date_adhesion = $date_adhesion; 
                    $carteMembre->date_expiration = $date_expiration;
                    $carteMembre->save(); 
                     
                /*
                    $model = new Migration(); 
                    $model->file_name = $file->name;
                    $model->is_migrate = 0; 
                    $model->is_delete = 0; 
                    $model->date_upload = date('Y-m-d h:m:s');
                    $model->migrate_by = Yii::$app->user->identity->username;
                    $model->save();
                 * 
                 */
                  
                echo \yii\helpers\Json::encode($file);
            }
                
            /*    
                
            }else{
                $id_empr = null;
            }
             * 
             */
            //Print file data
           // print_r($file);

            
    }else{
        
        return $this->renderAjax('upload'); 
        
        
    }

    return false;
    }
    
    public function actionGetLisLekte(){
        echo $this->renderAjax('listlekte'); 
    }
    
    public function actionGetLisLekteSearch($value){
        echo $this->renderAjax('listlektesearch',['value'=>$value]); 
    }
    
    public function actionGetLisEnprime($value){
        echo $this->renderAjax('lis-kat-enprime',['value'=>$value]); 
    }
    
    public function actionCartepdf($listid){
        //$this->layout = "pdf_layout";
        $content = $this->renderPartial('cartepdf',['listid'=>$listid]);
        $pdf = new Pdf();
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s');
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
				//'SetHeader'=>['Donnee Enquetes Avril 2015'],
				'SetFooter'=>['{PAGENO}'],
			];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
        $array_id = explode(",",$listid); 
        for($i=0;$i<sizeof($array_id);$i++){
            $carte = LogipamCarteMembre::findOne($array_id[$i]);
            $carte->is_print = 1;
            $carte->date_print = date("y-m-d h:i:s");
            $carte->save();
        }
    	return $pdf->render();

        
    }
    
    public function actionPrintonecard($id){
        $this->layout = "pdf_layout";
        $content = $this->renderPartial('printonecard',['id'=>$id]);
        $pdf = new Pdf();
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s');
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = $pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
	    	'SetHeader'=>[''],
	    	'SetFooter'=>[''],
    	];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
    	return $pdf->render();

        
    }
    
    public function actionCartehtml($listid){
        $this->layout = "pdf_layout";
        return $this->render('cartehtml',['listid'=>$listid]);
    }
    
    public function actionDeletecarte($id){
        $carte = LogipamCarteMembre::findOne($id);
        $file_name = $carte->image_name;
        $file_to_delete = '../../../../photos/lecteurs/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'.$file_to_delete.'"');
        $carte->delete(); 
        
    }
    
    public function actionDeletekat($id){
        $sql = "SELECT * FROM logipam_carte_membre WHERE empr_cb = '$id'"; 
        $carte = LogipamCarteMembre::findBySql($sql)->all();
        $file_name = $carte->image_name;
        $id_kat = $carte->id;
        $carte1 = LogipamCarteMembre::findOne($id_kat);
        $file_to_delete = '../../../../photos/lecteurs/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'.$file_to_delete.'"');
        $carte1->delete(); 
        
    }
    
    // for jpg 
    /**
     * 
     * @param type $file
     * @param type $w
     * @param type $h
     * @return type
     */
public function resize_imagejpg($file, $w, $h) {
   list($width, $height) = getimagesize($file);
   $src = imagecreatefromjpeg($file);
   $dst = imagecreatetruecolor($w, $h);
   imagecopyresampled($dst, $src, 0, 0, 0, 0, $w, $h, $width, $height);
   return $dst;
}
    
}