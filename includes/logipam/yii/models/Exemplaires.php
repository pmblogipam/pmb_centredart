<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exemplaires".
 *
 * @property int $expl_id
 * @property string $expl_cb
 * @property int $expl_notice
 * @property int $expl_bulletin
 * @property int $expl_typdoc
 * @property string $expl_cote
 * @property int $expl_section
 * @property int $expl_statut
 * @property int $expl_location
 * @property int $expl_codestat
 * @property string $expl_date_depot
 * @property string $expl_date_retour
 * @property string $expl_note
 * @property string $expl_prix
 * @property int $expl_owner
 * @property int $expl_lastempr
 * @property string $last_loan_date
 * @property string $create_date
 * @property string $update_date
 * @property int $type_antivol
 * @property int $transfert_location_origine
 * @property int $transfert_statut_origine
 * @property string $expl_comment
 * @property int $expl_nbparts
 * @property int $expl_retloc
 * @property int $expl_abt_num
 * @property int $transfert_section_origine
 * @property int $expl_ref_num
 */
class Exemplaires extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exemplaires';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expl_notice', 'expl_bulletin', 'expl_typdoc', 'expl_section', 'expl_statut', 'expl_location', 'expl_codestat', 'expl_owner', 'expl_lastempr', 'type_antivol', 'transfert_location_origine', 'transfert_statut_origine', 'expl_nbparts', 'expl_retloc', 'expl_abt_num', 'transfert_section_origine', 'expl_ref_num'], 'integer'],
            [['expl_date_depot', 'expl_date_retour', 'last_loan_date', 'create_date', 'update_date'], 'safe'],
            [['expl_note'], 'required'],
            [['expl_note', 'expl_comment'], 'string'],
            [['expl_cb', 'expl_cote', 'expl_prix'], 'string', 'max' => 255],
            [['expl_cb'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'expl_id' => 'Expl ID',
            'expl_cb' => 'Expl Cb',
            'expl_notice' => 'Expl Notice',
            'expl_bulletin' => 'Expl Bulletin',
            'expl_typdoc' => 'Expl Typdoc',
            'expl_cote' => 'Expl Cote',
            'expl_section' => 'Expl Section',
            'expl_statut' => 'Expl Statut',
            'expl_location' => 'Expl Location',
            'expl_codestat' => 'Expl Codestat',
            'expl_date_depot' => 'Expl Date Depot',
            'expl_date_retour' => 'Expl Date Retour',
            'expl_note' => 'Expl Note',
            'expl_prix' => 'Expl Prix',
            'expl_owner' => 'Expl Owner',
            'expl_lastempr' => 'Expl Lastempr',
            'last_loan_date' => 'Last Loan Date',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'type_antivol' => 'Type Antivol',
            'transfert_location_origine' => 'Transfert Location Origine',
            'transfert_statut_origine' => 'Transfert Statut Origine',
            'expl_comment' => 'Expl Comment',
            'expl_nbparts' => 'Expl Nbparts',
            'expl_retloc' => 'Expl Retloc',
            'expl_abt_num' => 'Expl Abt Num',
            'transfert_section_origine' => 'Transfert Section Origine',
            'expl_ref_num' => 'Expl Ref Num',
        ];
    }
}
