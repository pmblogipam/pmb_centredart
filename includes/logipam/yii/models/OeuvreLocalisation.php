<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oeuvre_localisation".
 *
 * @property int $id
 * @property string $nom_localisation
 * @property string $cree_par
 * @property string $modifie_par
 * @property string $date_creation
 */
class OeuvreLocalisation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oeuvre_localisation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_localisation'], 'required'],
            [['date_creation'], 'safe'],
            [['nom_localisation', 'cree_par', 'modifie_par'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nom_localisation' => 'Nom Localisation',
            'cree_par' => 'Cree Par',
            'modifie_par' => 'Modifie Par',
            'date_creation' => 'Date Creation',
        ];
    }
}
