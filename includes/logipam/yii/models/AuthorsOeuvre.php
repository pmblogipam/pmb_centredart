<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors_oeuvre".
 *
 * @property int $id
 * @property string $prenom
 * @property string $nom
 * @property string $date_person
 * @property string $index_person
 * @property int $type 1: Personne lie a l'acquisition; 2: Acheteur; 3:Nom restaurateur
 * @property string $cree_par
 * @property string $date_creation
 * @property string $modifie_par
 * @property string $date_modification
 */
class AuthorsOeuvre extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors_oeuvre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index_person'], 'string'],
            [['type'], 'integer'],
            [['date_creation', 'date_modification'], 'safe'],
            [['prenom', 'nom', 'cree_par', 'modifie_par'], 'string', 'max' => 255],
            [['date_person'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prenom' => 'Prenom',
            'nom' => 'Nom',
            'date_person' => 'Date Person',
            'index_person' => 'Index Person',
            'type' => 'Type',
            'cree_par' => 'Cree Par',
            'date_creation' => 'Date Creation',
            'modifie_par' => 'Modifie Par',
            'date_modification' => 'Date Modification',
        ];
    }
    
    public function getFullName($id){
        $model = AuthorsOeuvre::findOne($id); 
        if($id==null){
            return 'Aucune donnée'; 
        }else{
            return $model->prenom.' '.$model->nom; 
        }
    }

}
