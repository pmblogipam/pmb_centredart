<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oeuvre_physique".
 *
 * @property int $id
 * @property int $notice_id
 * @property string $localisation
 * @property string $emplacement
 * @property string $etat_conservation
 * @property string $constat
 * @property int $nom_restaurateur
 * @property string $date_restauration
 * @property string $object_restauration
 * @property string $cree_par
 * @property string $date_creation
 * @property string $modifier_par
 * @property string $date_modification
 */
class OeuvrePhysique extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oeuvre_physique';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id'], 'required'],
            [['notice_id', 'nom_restaurateur'], 'integer'],
            [['constat', 'object_restauration'], 'string'],
            [['date_restauration', 'date_creation', 'date_modification'], 'safe'],
            [['localisation', 'emplacement', 'etat_conservation', 'cree_par', 'modifier_par'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notice_id' => 'Notice ID',
            'localisation' => 'Localisation',
            'emplacement' => 'Emplacement',
            'etat_conservation' => 'Etat Conservation',
            'constat' => 'Constat',
            'nom_restaurateur' => 'Nom Restaurateur',
            'date_restauration' => 'Date Restauration',
            'object_restauration' => 'Object Restauration',
            'cree_par' => 'Cree Par',
            'date_creation' => 'Date Creation',
            'modifier_par' => 'Modifier Par',
            'date_modification' => 'Date Modification',
        ];
    }
}
