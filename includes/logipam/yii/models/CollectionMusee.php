<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collection_musee".
 *
 * @property int $id
 * @property string $nom_musee
 * @property string $adresse_musee
 * @property string $phone_musee
 * @property string $date_creation
 * @property string $cree_par
 * @property string $date_modification
 * @property string $modifier_par
 */
class CollectionMusee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection_musee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_musee'], 'required'],
            [['date_creation', 'date_modification'], 'safe'],
            [['nom_musee', 'adresse_musee'], 'string', 'max' => 255],
            [['phone_musee'], 'string', 'max' => 32],
            [['cree_par', 'modifier_par'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nom_musee' => 'Nom Musee',
            'adresse_musee' => 'Adresse Musee',
            'phone_musee' => 'Phone Musee',
            'date_creation' => 'Date Creation',
            'cree_par' => 'Cree Par',
            'date_modification' => 'Date Modification',
            'modifier_par' => 'Modifier Par',
        ];
    }
}
