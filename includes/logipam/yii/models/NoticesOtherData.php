<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notices_other_data".
 *
 * @property int $id
 * @property int $notices_id
 * @property string $lieu_creation
 * @property double $longue
 * @property double $larg
 * @property double $haut
 * @property string $materiaux
 * @property string $technique
 * @property string $description
 * @property string $legende_image
 * @property string $creer_par
 * @property string $date_creation
 * @property string $modifier_par
 * @property string $date_modification
 */
class NoticesOtherData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notices_other_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notices_id'], 'required'],
            [['notices_id'], 'integer'],
            [['longue', 'larg', 'haut'], 'number'],
            [['description'], 'string'],
            [['date_creation', 'date_modification'], 'safe'],
            [['lieu_creation', 'materiaux', 'technique', 'legende_image'], 'string', 'max' => 255],
            [['creer_par', 'modifier_par'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notices_id' => 'Notices ID',
            'lieu_creation' => 'Lieu Creation',
            'longue' => 'Longue',
            'larg' => 'Larg',
            'haut' => 'Haut',
            'materiaux' => 'Materiaux',
            'technique' => 'Technique',
            'description' => 'Description',
            'legende_image' => 'Legende Image',
            'creer_par' => 'Creer Par',
            'date_creation' => 'Date Creation',
            'modifier_par' => 'Modifier Par',
            'date_modification' => 'Date Modification',
        ];
    }
}
