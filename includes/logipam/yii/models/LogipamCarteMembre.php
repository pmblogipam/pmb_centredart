<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logipam_carte_membre".
 *
 * @property integer $id
 * @property string $empr_cb
 * @property string $prenom
 * @property string $nom
 * @property string $sexe
 * @property string $date_adhesion
 * @property string $date_expiration
 * @property string $image_name
 * @property string $date_ajout
 * @property integer $is_print
 * @property string $date_print
 */
class LogipamCarteMembre extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logipam_carte_membre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empr_cb', 'prenom', 'nom', 'sexe', 'date_adhesion', 'date_expiration', 'image_name', 'date_ajout'], 'required'],
            [['date_adhesion', 'date_expiration', 'date_ajout', 'date_print'], 'safe'],
            [['is_print'], 'integer'],
            [['empr_cb'], 'string', 'max' => 255],
            [['prenom', 'nom', 'sexe', 'image_name'], 'string', 'max' => 64],
            [['empr_cb'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empr_cb' => 'Empr Cb',
            'prenom' => 'Prenom',
            'nom' => 'Nom',
            'sexe' => 'Sexe',
            'date_adhesion' => 'Date Adhesion',
            'date_expiration' => 'Date Expiration',
            'image_name' => 'Image Name',
            'date_ajout' => 'Date Ajout',
            'is_print' => 'Is Print',
            'date_print' => 'Date Print',
        ];
    }
}
