<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collection_emprunteur".
 *
 * @property int $id
 * @property string $prenom_empr
 * @property string $nom_empr
 * @property string $phone_empr
 * @property string $adresse_empr
 * @property string $date_creation
 * @property string $cree_par
 * @property string $date_modification
 * @property string $modifie_par
 */
class CollectionEmprunteur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection_emprunteur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prenom_empr', 'nom_empr'], 'required'],
            [['date_creation', 'date_modification'], 'safe'],
            [['prenom_empr', 'nom_empr', 'adresse_empr', 'cree_par', 'modifie_par'], 'string', 'max' => 255],
            [['phone_empr'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prenom_empr' => 'Prenom Empr',
            'nom_empr' => 'Nom Empr',
            'phone_empr' => 'Phone Empr',
            'adresse_empr' => 'Adresse Empr',
            'date_creation' => 'Date Creation',
            'cree_par' => 'Cree Par',
            'date_modification' => 'Date Modification',
            'modifie_par' => 'Modifie Par',
        ];
    }
}
