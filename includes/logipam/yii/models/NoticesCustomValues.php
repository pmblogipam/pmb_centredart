<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notices_custom_values".
 *
 * @property int $notices_custom_champ
 * @property int $notices_custom_origine
 * @property string $notices_custom_small_text
 * @property string $notices_custom_text
 * @property int $notices_custom_integer
 * @property string $notices_custom_date
 * @property double $notices_custom_float
 * @property int $notices_custom_order
 */
class NoticesCustomValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notices_custom_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notices_custom_champ', 'notices_custom_origine', 'notices_custom_integer', 'notices_custom_order'], 'integer'],
            [['notices_custom_text'], 'string'],
            //[['notices_custom_date'], 'safe'],
            [['notices_custom_float'], 'number'],
            [['notices_custom_small_text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notices_custom_champ' => 'Notices Custom Champ',
            'notices_custom_origine' => 'Notices Custom Origine',
            'notices_custom_small_text' => 'Notices Custom Small Text',
            'notices_custom_text' => 'Notices Custom Text',
            'notices_custom_integer' => 'Notices Custom Integer',
            'notices_custom_date' => 'Notices Custom Date',
            'notices_custom_float' => 'Notices Custom Float',
            'notices_custom_order' => 'Notices Custom Order',
        ];
    }
    
     
}
