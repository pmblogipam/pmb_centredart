<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oeuvre_provenance".
 *
 * @property int $id
 * @property int $notice_id
 * @property string $type_acquisition
 * @property int $person_acquisition
 * @property string $date_acquisition
 * @property double $prix_achat
 * @property double $valeur_assurance
 * @property string $vente
 * @property string $date_vente
 * @property int $acheteur
 * @property double $prix_vente
 * @property string $cree_par
 * @property string $date_creation
 * @property string $modifier_par
 * @property string $date_modification
 */
class OeuvreProvenance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oeuvre_provenance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id'], 'required'],
            [['notice_id', 'person_acquisition', 'acheteur'], 'integer'],
            [['date_acquisition', 'date_vente', 'date_creation', 'date_modification'], 'safe'],
            [['prix_achat', 'valeur_assurance', 'prix_vente'], 'number'],
            [['vente'], 'string'],
            [['type_acquisition', 'cree_par', 'modifier_par'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notice_id' => 'Notice ID',
            'type_acquisition' => 'Type Acquisition',
            'person_acquisition' => 'Person Acquisition',
            'date_acquisition' => 'Date Acquisition',
            'prix_achat' => 'Prix Achat',
            'valeur_assurance' => 'Valeur Assurance',
            'vente' => 'Vente',
            'date_vente' => 'Date Vente',
            'acheteur' => 'Acheteur',
            'prix_vente' => 'Prix Vente',
            'cree_par' => 'Cree Par',
            'date_creation' => 'Date Creation',
            'modifier_par' => 'Modifier Par',
            'date_modification' => 'Date Modification',
        ];
    }
    
    
}
