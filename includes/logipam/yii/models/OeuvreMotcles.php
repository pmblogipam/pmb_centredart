<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oeuvre_motcles".
 *
 * @property int $id
 * @property int $notice_id
 * @property string $mots_cles
 * @property string $cree_par
 * @property string $date_creation
 * @property string $modifie_par
 * @property string $date_modification
 */
class OeuvreMotcles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oeuvre_motcles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id'], 'required'],
            [['notice_id'], 'integer'],
            [['mots_cles'], 'string'],
            [['date_creation', 'date_modification'], 'safe'],
            [['cree_par', 'modifie_par'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notice_id' => 'Notice ID',
            'mots_cles' => 'Mots Cles',
            'cree_par' => 'Cree Par',
            'date_creation' => 'Date Creation',
            'modifie_par' => 'Modifie Par',
            'date_modification' => 'Date Modification',
        ];
    }
}
