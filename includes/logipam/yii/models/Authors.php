<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property int $author_id
 * @property string $author_type
 * @property string $author_name
 * @property string $author_rejete
 * @property string $author_date
 * @property int $author_see
 * @property string $author_web
 * @property string $index_author
 * @property string $author_comment
 * @property string $author_lieu
 * @property string $author_ville
 * @property string $author_pays
 * @property string $author_subdivision
 * @property string $author_numero
 * @property int $author_import_denied
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_type', 'index_author', 'author_comment'], 'string'],
            [['author_see', 'author_import_denied'], 'integer'],
            [['author_name', 'author_rejete', 'author_date', 'author_web', 'author_lieu', 'author_ville', 'author_pays', 'author_subdivision'], 'string', 'max' => 255],
            [['author_numero'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'author_id' => 'Author ID',
            'author_type' => 'Author Type',
            'author_name' => 'Author Name',
            'author_rejete' => 'Author Rejete',
            'author_date' => 'Author Date',
            'author_see' => 'Author See',
            'author_web' => 'Author Web',
            'index_author' => 'Index Author',
            'author_comment' => 'Author Comment',
            'author_lieu' => 'Author Lieu',
            'author_ville' => 'Author Ville',
            'author_pays' => 'Author Pays',
            'author_subdivision' => 'Author Subdivision',
            'author_numero' => 'Author Numero',
            'author_import_denied' => 'Author Import Denied',
        ];
    }
}
