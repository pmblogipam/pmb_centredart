<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notices_keywords".
 *
 * @property int $id
 * @property string $mots_cles
 * @property string $creer_par
 * @property string $date_creation
 * @property string $modifie_par
 * @property string $date_modification
 */
class NoticesKeywords extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notices_keywords';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mots_cles'], 'required'],
            [['date_creation', 'date_modification'], 'safe'],
            [['mots_cles'], 'string', 'max' => 255],
            [['creer_par', 'modifie_par'], 'string', 'max' => 64],
            [['mots_cles'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mots_cles' => 'Mots Cles',
            'creer_par' => 'Creer Par',
            'date_creation' => 'Date Creation',
            'modifie_par' => 'Modifie Par',
            'date_modification' => 'Date Modification',
        ];
    }
}
