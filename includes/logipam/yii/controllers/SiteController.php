<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\LogipamPresence; 
use app\models\Empr; 
use app\models\LogipamCarteMembre; 
use kartik\mpdf\Pdf; // For PDF 
use app\models\Authors; 
use app\models\NoticesKeywords; 
use app\models\AuthorsOeuvre; 
use app\models\Notices; 
use app\models\Exemplaires; 
use app\models\Responsability; 
use app\models\OeuvreMotcles; 
use app\models\OeuvrePhysique; 
use app\models\OeuvreProvenance;
use app\models\NoticesCustomValues; 
use app\models\NoticesOtherData; 
use app\models\OeuvreLocalisation; 
use app\models\CollectionMusee;
use app\models\CollectionEmprunteur;


class SiteController extends Controller
{
    
    public function getUsername(){
        if(isset($_SERVER['HTTP_COOKIE'])){
        $array_sess = explode(";",$_SERVER['HTTP_COOKIE']); 
        $user_pmb = explode("=",$array_sess[2]); 
   
        return  $user_pmb[1];
        }else{
            return null; 
        }
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "layout_pmb";
        return $this->render('pmb');
    }
    
    public function actionMotscles()
    {
        $this->layout = "pdf_layout";
        return $this->render('motscles');
    }
    
    public function actionGetSearch(){
       return $this->renderAjax('search'); 
    }
    
    public function actionLastOeuvre(){
        return $this->renderAjax('last-oeuvre');
    }
    
    public function actionPretExpo(){
        $all_notices = Notices::findBySql("SELECT CONCAT(tit1,' (',code,')') AS 'name', notice_id AS 'id' FROM notices WHERE typdoc NOT IN ('a','b')  AND code IS NOT NULL AND tit1 IS NOT NULL ORDER BY tit1")->asArray()->all();
        $json_notices = json_encode($all_notices,false);
        return $this->renderAjax('pret-expo',['json_notices'=>$json_notices]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    public function actionGetLecteurInfo($code){
       $count_lecteur = Empr::find()->where(['=','empr_cb',$code])->count(); 
       if($count_lecteur==0){
           echo $this->renderAjax('error_lecteur',['code'=>$code]);
       }else{
           echo $this->renderAjax('empr',['code'=>$code]);
       }
        
       
    }
    
    public function actionGetRaportParam($date1, $date2){
        echo $this->renderAjax('raport_param',['date1'=>$date1,'date2'=>$date2]);
    }
    
    
    public function actionGetRapportVisite(){
       
      echo  $this->renderAjax('raport');  
    }
    
    public function actionGetHeure(){
        echo date('Y-m-d h:i:s');
    }
    
    /**
     * 
     * 
     */
    public function actionGetTotalVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $visite_par_jour = LogipamPresence::find()->where(['=','date_presence',$today])->count();  
        echo $visite_par_jour; 
    }
    
    public function actionGetTotalFemaleVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = 2 AND lp.date_presence = '$today'";
        $visite_female_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_female_jour; 
    }
    
    public function actionGetTotalMaleVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = 1 AND lp.date_presence = '$today'";
        $visite_male_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_male_jour; 
    }
    
    public function actionGetAdhesionDepassee(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id_empr, empr_date_expiration FROM empr WHERE empr_date_expiration <= '$today'"; 
        $total_adhesion_depassee = LogipamPresence::findBySql($sql_str)->count(); 
        echo $total_adhesion_depassee; 
    }
    
    public function actionGetRapportListe(){
        echo $this->renderAjax('liste');
    }
    
    
  public function actionUpload(){
        $fileName = 'file';
        $uploadPath = '../../../../photos/lecteurs';
       
        if (isset($_FILES[$fileName])) {
            
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            
           // echo $empr_code; 
            /*
            if(isset($data_empr)){
                $empr_cb = $empr_code; 
                $id_empr = $data_empr->id_empr;
                $prenom = $data_empr->empr_prenom;
                $nom = $data_empr->empr_nom; 
                $sexe = $data_empr->empr_sexe; 
                $date_adhesion = $data_empr->empr_date_adhesion;
                $date_expiration = $data_empr->empr_date_expiration;
              */  
                
            
                if ($file->saveAs($uploadPath . '/' . $file->name)) {
                   
                   $file_path = $uploadPath . '/' . $file->name;
                    // jpg  change the dimension 750, 450 to your desired values
                   $img = $this->resize_imagejpg($file_path, 200, 200);
                    // again for jpg
                    imagejpeg($img, $uploadPath . '/' . $file->name);
                   // $img = resize_imagejpg($file_path, 200, 200);
                    $carteMembre = new LogipamCarteMembre();
                   // $data_empr = new Empr();
                    $empr_code = substr($file->name,0,-4);
                    
                   $data_empr = Empr::findBySql("SELECT empr_prenom, empr_nom, empr_sexe, empr_date_adhesion, empr_date_expiration FROM empr WHERE empr_cb = '$empr_code'")->asArray()->all();
                    //print_r($data_empr);
                   // $empr_cb = $empr_code; 
                    //$id_empr = $data_empr->id_empr;
                           
                    $prenom = $data_empr[0]['empr_prenom'];
                    $nom = $data_empr[0]['empr_nom']; 
                    $sexe = $data_empr[0]['empr_sexe']; 
                    $date_adhesion = $data_empr[0]['empr_date_adhesion'];
                    $date_expiration = $data_empr[0]['empr_date_expiration'];
                    
                    
                //Now save file data to database
                  
                    $carteMembre->empr_cb = $empr_code; 
                    $carteMembre->image_name = $file->name;//$dataempr->getPrenom($empr_code);
                    $carteMembre->date_ajout = date('Y-m-d h:m:s');
                    $carteMembre->prenom = $prenom; 
                    $carteMembre->nom = $nom; 
                    $carteMembre->sexe = $sexe; 
                    $carteMembre->date_adhesion = $date_adhesion; 
                    $carteMembre->date_expiration = $date_expiration;
                    $carteMembre->save(); 
                     
                /*
                    $model = new Migration(); 
                    $model->file_name = $file->name;
                    $model->is_migrate = 0; 
                    $model->is_delete = 0; 
                    $model->date_upload = date('Y-m-d h:m:s');
                    $model->migrate_by = Yii::$app->user->identity->username;
                    $model->save();
                 * 
                 */
                  
                echo \yii\helpers\Json::encode($file);
            }
                
            /*    
                
            }else{
                $id_empr = null;
            }
             * 
             */
            //Print file data
           // print_r($file);

            
    }else{
        
        return $this->renderAjax('upload'); 
        
        
    }

    return false;
    }
    
    public function actionGetLisLekte(){
        echo $this->renderAjax('listlekte'); 
    }
    
    public function actionGetLisLekteSearch($value){
        echo $this->renderAjax('listlektesearch',['value'=>$value]); 
    }
    
    public function actionGetLisEnprime($value){
        echo $this->renderAjax('lis-kat-enprime',['value'=>$value]); 
    }
    
    public function actionCartepdf($listid){
        //$this->layout = "pdf_layout";
        $content = $this->renderPartial('cartepdf',['listid'=>$listid]);
        $pdf = new Pdf();
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s');
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
				//'SetHeader'=>['Donnee Enquetes Avril 2015'],
				'SetFooter'=>['{PAGENO}'],
			];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
        $array_id = explode(",",$listid); 
        for($i=0;$i<sizeof($array_id);$i++){
            $carte = LogipamCarteMembre::findOne($array_id[$i]);
            $carte->is_print = 1;
            $carte->date_print = date("y-m-d h:i:s");
            $carte->save();
        }
    	return $pdf->render();

        
    }
    
    public function actionPrintonecard($id){
        $this->layout = "pdf_layout";
        $content = $this->renderPartial('printonecard',['id'=>$id]);
        $pdf = new Pdf();
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s');
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = $pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
	    	'SetHeader'=>[''],
	    	'SetFooter'=>[''],
    	];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
    	return $pdf->render();

        
    }
    
    public function actionCartehtml($listid){
        $this->layout = "pdf_layout";
        return $this->render('cartehtml',['listid'=>$listid]);
    }
    // Formulaire de creation de l'oeuvre 
    
    public function actionCreateOeuvre(){
        $all_authors = Authors::findBySql("SELECT CONCAT(author_name,' ',author_rejete) AS 'name', author_id AS 'id' FROM authors ORDER BY author_name")->asArray()->all();
        $all_keywords = NoticesKeywords::findBySql("SELECT mots_cles FROM notices_keywords ORDER BY mots_cles ASC")->asArray()->all();
        $all_authors_oeuvre = AuthorsOeuvre::findBySql("SELECT CONCAT(nom,' ',prenom) AS 'name', id AS 'id' FROM authors_oeuvre ORDER BY nom")->asArray()->all();
        $all_localisation = OeuvreLocalisation::findBySql("SELECT nom_localisation AS 'name', id AS 'id' FROM oeuvre_localisation ORDER BY nom_localisation")->asArray()->all();
        
        $key_words = []; 
        $i=0;
        foreach($all_keywords as $k){
            $key_words[$i] = $k['mots_cles'];
            $i++;
        }
        $string_key = '["'.implode('","', $key_words).'"]';
        
       $json_authors =json_encode($all_authors,false); 
       $json_authors_oeuvre = json_encode($all_authors_oeuvre,false); 
       $json_localisation = json_encode($all_localisation,false);
        return $this->renderAjax('create-oeuvre',['json_authors'=>$json_authors,'js_all_keywords'=>$string_key,'json_authors_oeuvre'=>$json_authors_oeuvre,'json_localisation'=>$json_localisation]); 
    }
    
    
    
   
   /**
    * Enregistrer un artiste 
    * @param type $prenom
    * @param type $nom
    * @param type $date
    * @return type notice_id 
    */
    public function actionAddArtiste($prenom, $nom, $date){
        $authors = new Authors(); 
        $authors->author_type = '70';
        $authors->author_name = strtoupper($nom); 
        $authors->author_rejete = $prenom;
        $authors->author_date = $date;
        $authors->author_see = 0; 
        $authors->index_author = strtolower($nom.' '.$prenom); 
        $authors->author_web = ''; 
        $authors->author_lieu = ''; 
        $authors->author_ville = ''; 
        $authors->author_pays = ''; 
        $authors->author_subdivision = '';
        $authors->author_numero = ''; 
        $authors->author_import_denied = 0; 
        
        if($authors->save())
            { 
                return $authors->author_id; 
        
        }else{
            return null;
        }
   }
   
   public function actionAddMusee($nom_musee,$adresse_musee,$phone_musee){
       $musee = new CollectionMusee();
       $musee->nom_musee = $nom_musee;
       $musee->adresse_musee = $adresse_musee; 
       $musee->phone_musee = $phone_musee; 
       $musee->date_creation = date('Y-m-d h:i:s');
       $musee->cree_par = $this->getUsername();
       if($musee->save()){
           return $musee->id; 
       }else{
           return null;
       }
       
   }
   
   public function actionAddEmprunteur($prenom_empr,$nom_empr,$phone_empr,$adresse_empr){
       $empr = new CollectionEmprunteur(); 
       $empr->prenom_empr = $prenom_empr;
       $empr->nom_empr = $nom_empr;
       $empr->phone_empr = $phone_empr;
       $empr->adresse_empr = $adresse_empr; 
       $empr->date_creation = date('Y-m-d h:i:s');
       $empr->cree_par = $this->getUsername();
       if($empr->save()){
           return $empr->id;
       }else{
           return null;
       }
   }
   
   /**
    * 
    * @param \app\controllers\type $mots
    * @return \app\controllers\type
    * @param type $mots
    * @return type
    */
   public function actionAddKeywords($mots){
       
       $keywords = new NoticesKeywords(); 
       
       $keywords->mots_cles = $mots;
       $keywords->creer_par = $this->getUsername(); 
       if($keywords->save()){
           return $mots; 
       }else{
           return null;
       }
   }
   
   /**
    * 
    * @param type $nom_localisation
    * @return type
    */
   public function actionAddLocalisation($nom_localisation){
       $oeuvre_localisation = new OeuvreLocalisation(); 
       $oeuvre_localisation->nom_localisation = $nom_localisation; 
       $oeuvre_localisation->cree_par = $this->getUsername(); 
       $oeuvre_localisation->date_creation = date('Y-m-d h:i:s');
       if($oeuvre_localisation->save()){
           return $nom_localisation; 
       }else{
           return null;
       }
   }
   
   public function actionAddAuthorOeuvre($prenom, $nom, $date,$type){
       $authors_oeuvre = new AuthorsOeuvre(); 
       $authors_oeuvre->prenom = $prenom; 
       $authors_oeuvre->nom = $nom; 
       $authors_oeuvre->date_person = $date; 
       $authors_oeuvre->type = $type; 
       $authors_oeuvre->cree_par = $this->getUsername(); 
       $authors_oeuvre->date_creation = date('Y-m-d h:i:s');
       $authors_oeuvre->index_person = strtolower($nom.' '.$prenom); 
       if($authors_oeuvre->save()){
           return $authors_oeuvre->id; 
       }else{
           return null;
       }
   }
   
   public function actionSaveOeuvre(
           $no_inventaire, 
           $ancien_no_inventaire,
           $categorie,
           $titre,
           $signature,
           $titre_provisoire,
           $date_creation,
           $statut,
           $lieu_creation,
           $longue,
           $larg,
           $haut,
           $materiaux, 
           $technique,
           $description,
           $legende_image,
           $artiste,
           $mots_cles,
           $type_acquisition,
           $person_acquisition,
           $date_acquisition,
           $prix_achat,
           $valeur_assurance,
           $vente,
           $date_vente,
           $acheteur,
           $prix_vente,
           $localisation,
           $emplacement,
           $etat_conservation,
           $constat,
           $nom_restaurateur,
           $date_restauration,
           $object_restauration
            
           ){
       
       $notices = new Notices(); 
       $exemplaires = new Exemplaires(); 
       
       $responsability = new Responsability(); 
       $oeuvre_mot_cles = new OeuvreMotcles(); 
       $oeuvre_physique = new OeuvrePhysique(); 
       $oeuvre_provenance = new OeuvreProvenance();
       $notices_custom_values = new NoticesCustomValues();
       $notices_other_data = new NoticesOtherData(); 
       // Debut de la transaction d'enregistrement d'une oeuvre
       $is_oeuvre_save = FALSE; 
       $dbTrans = Yii::$app->db->beginTransaction(); 
       
       $notices->code = $no_inventaire;
       $notices->typdoc = $categorie; 
       $notices->tit1 = $titre;
       $notices->tit2 = $titre_provisoire;
       $notices->date_parution = date("Y-m-d h:i:s",strtotime($date_creation));  
       $notices->tnvol = "";  
       $notices->mention_edition = ""; 
       $notices->eformat = ""; 
       $notices->prix = ""; 
       $notices->signature = $signature; 
       $notices->indexation_lang = "";
       $notices->map_equinoxe = "";
       
       if($notices->save()){
           $notices_custom_values->notices_custom_champ = 5; 
           $notices_custom_values->notices_custom_origine = $notices->notice_id;
           $notices_custom_values->notices_custom_small_text = $ancien_no_inventaire;
           
           if($notices_custom_values->save()){
               $notices_custom_values = new NoticesCustomValues();
               $notices_custom_values->notices_custom_champ = 1; 
               $notices_custom_values->notices_custom_origine = $notices->notice_id;
               $notices_custom_values->notices_custom_small_text = $statut;
               $notices_custom_values->save();
               $is_oeuvre_save = TRUE;
           }else{
               $is_oeuvre_save = FALSE;
           }
           // Sauvegarde des autres elements de l'oeuvre 
           $notices_other_data->notices_id = $notices->notice_id;
           $notices_other_data->lieu_creation = $lieu_creation; 
           $notices_other_data->longue = $longue; 
           $notices_other_data->larg = $larg;
           $notices_other_data->haut = $haut;
           $notices_other_data->materiaux = $materiaux; 
           $notices_other_data->technique = $technique;
           $notices_other_data->description = $description; 
           $notices_other_data->legende_image = $legende_image;
           $notices_other_data->creer_par = $this->getUsername(); 
           $notices_other_data->date_creation = date('Y-m-d h:i:s');
           $notices_other_data->save();
           
           // Enregistrement des responsabilites 
           $responsability->responsability_author = $artiste; 
           $responsability->responsability_notice = $notices->notice_id; 
           $responsability->responsability_fonction = '070';
           $responsability->save(); 
           
           // Enregistrement des mots cles de l'oeuvre 
           $oeuvre_mot_cles->notice_id = $notices->notice_id; 
           $oeuvre_mot_cles->mots_cles = $mots_cles; 
           $oeuvre_mot_cles->cree_par = $this->getUsername(); 
           $oeuvre_mot_cles->date_creation = date('Y-m-d h:i:s');
           $oeuvre_mot_cles->save();
           
           // Enregistrement des infos de la provenance de l'oeuvre 
           $oeuvre_provenance->notice_id = $notices->notice_id;
           $oeuvre_provenance->type_acquisition = $type_acquisition; 
           $oeuvre_provenance->person_acquisition = $person_acquisition; 
           $oeuvre_provenance->date_acquisition = date("Y-m-d h:i:s",strtotime($date_acquisition));
           $oeuvre_provenance->prix_achat = $prix_achat; 
           $oeuvre_provenance->valeur_assurance = $valeur_assurance; 
           $oeuvre_provenance->vente = $vente; 
           $oeuvre_provenance->date_vente = date("Y-m-d h:i:s",strtotime($date_vente));
           $oeuvre_provenance->acheteur = $acheteur;
           $oeuvre_provenance->prix_vente = $prix_vente; 
           $oeuvre_provenance->cree_par = $this->getUsername(); 
           $oeuvre_provenance->date_creation = date('Y-m-d h:i:s');
           $oeuvre_provenance->save();
           
           // Enregistrement des infos physique de l'oeuvre
           $oeuvre_physique->notice_id = $notices->notice_id;
           $oeuvre_physique->localisation = $localisation; 
           $oeuvre_physique->emplacement = $emplacement; 
           $oeuvre_physique->etat_conservation = $etat_conservation; 
           $oeuvre_physique->constat = $constat; 
           $oeuvre_physique->nom_restaurateur = $nom_restaurateur;
           $oeuvre_physique->date_restauration = date("Y-m-d h:i:s",strtotime($date_restauration));
           $oeuvre_physique->object_restauration = $object_restauration; 
           $oeuvre_physique->cree_par = $this->getUsername(); 
           $oeuvre_physique->date_creation = date('Y-m-d h:i:s');
           $oeuvre_physique->save(); 
           
           
       }else{
           $is_oeuvre_save = FALSE;
       }
       
       if(!$is_oeuvre_save){
           $dbTrans->rollback();
           return 'Imposible d\'enregistrer l\'oeuvre une erreur a été constaté'; 
       }else{
           $dbTrans->commit();
            return $this->renderAjax('view-oeuvre',['notice_id'=>$notices->notice_id]);
       }
      }
      
    public function actionViewOeuvre($notice_id){
        return $this->renderAjax('view-oeuvre',['notice_id'=>$notice_id]); 
    }
    
    public function actionManageList(){
        $keywords = NoticesKeywords::findBySql("SELECT * FROM notices_keywords ORDER BY mots_cles ASC LIMIT 50")->asArray()->all();
        $person = AuthorsOeuvre::findBySql("SELECT * FROM authors_oeuvre ORDER BY nom ASC LIMIT 50")->asArray()->All();
        $localisation = OeuvreLocalisation::findBySql("SELECT * FROM oeuvre_localisation ORDER BY nom_localisation ASC LIMIT 50")->asArray()->All(); 
        return $this->renderAjax('manage-list',['keywords'=>$keywords,'person'=>$person,'localisation'=>$localisation]);
    }
    /**
     * Moteur de recherche des oeuvres 
     * @param type $search
     */
    public function actionSearchOeuvre($search){
        $all_oeuvre = []; 
        //SELECT n.notice_id, n.code, n.tit1, n.typdoc, CONCAT(a.author_rejete, ' ',a.author_name) AS 'nom_auteur'  FROM notices n INNER JOIN responsability r ON (r.responsability_notice = n.notice_id) INNER JOIN authors a ON (a.author_id = r.responsability_author) WHERE n.typdoc NOT IN ('a','b') ORDER BY notice_id DESC LIMIT 10
        $all_notices = Notices::findBySql("SELECT n.notice_id, n.code, n.tit1, n.typdoc, CONCAT(a.author_rejete, ' ',a.author_name) AS 'nom_auteur'  FROM notices n INNER JOIN responsability r ON (r.responsability_notice = n.notice_id) INNER JOIN authors a ON (a.author_id = r.responsability_author) WHERE n.typdoc NOT IN ('a','b') AND ( n.tit1 LIKE '%$search%' OR tit3 LIKE '%$search%' OR n.tit2 LIKE '%$search%' OR n.code LIKE '%$search%' OR n.signature LIKE '%$search%' OR a.index_author LIKE '%$search%')")->asArray()->all();
        if(!empty($all_notices)){
            $all_oeuvre = $all_notices; 
            return $this->renderAjax('result-search',['all_oeuvre'=>$all_oeuvre]); 
        }else{
            return $this->renderAjax('result-search',['all_oeuvre'=>$all_oeuvre]); 
        }
    }
    
    public function actionUpdateList(){
         if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
             $kote_pou_modifye = $_POST['name'];
              switch ($kote_pou_modifye){
                  case 'keywords':
                  {
                      $keywords = NoticesKeywords::findOne($_POST['pk']); 
                      $keywords->mots_cles = $_POST['value'];
                      $keywords->modifie_par = $this->getUsername();
                      $keywords->date_modification = date('Y-m-d h:i:s');
                      $keywords->save();
                  }
                  break;
                  case 'prenom':
                      {
                        $person = AuthorsOeuvre::findOne($_POST['pk']);
                        $person->prenom = $_POST['value'];
                        $person->index_person = strtolower($_POST['value']).' '.strtolower($person->nom);
                        $person->modifie_par = $this->getUsername();
                        $person->date_modification = date('Y-m-d h:i:s');
                        $person->save();
                      }
                      break;
                  case 'nom':
                  {
                        $person = AuthorsOeuvre::findOne($_POST['pk']);
                        $person->nom = $_POST['value'];
                        $person->index_person = strtolower($person->prenom).' '.strtolower($_POST['value']);
                        $person->modifie_par = $this->getUsername();
                        $person->date_modification = date('Y-m-d h:i:s');
                        $person->save();
                  }
                  break;
                  case 'localisation':
                  {
                      $localisation = OeuvreLocalisation::findOne($_POST['pk']); 
                      $localisation->nom_localisation = $_POST['value'];
                      $localisation->modifie_par = $this->getUsername();
                      $localisation->save();
                  }
              }
         }
    }
    
    public function actionDeleteKeyword($id){
        $keywords = NoticesKeywords::findOne($id); 
        if($keywords->delete()){
            echo "Success";
        }else{
            echo "Echec";
        } 
    }
    
    public function actionDeletePerson($id){
        $person = AuthorsOeuvre::findOne($id); 
        if($person->delete()){
            echo "Success";
        }else{
            echo "Echec";
        }
    }
    
    public function actionDeleteLocalisation($id){
        $localisation = OeuvreLocalisation::findOne($id); 
        if($localisation->delete()){
            echo "Success";
        }else{
            echo "Echec";
        }
    }
    
    public function actionRefreshKeyword(){
        $keywords = NoticesKeywords::findBySql("SELECT * FROM notices_keywords ORDER BY mots_cles ASC LIMIT 50")->asArray()->all(); 
        return $this->renderAjax('refresh-keyword',['keywords'=>$keywords]);
    }
    
    public function actionRefreshPerson(){
        $person = AuthorsOeuvre::findBySql("SELECT * FROM authors_oeuvre ORDER BY nom ASC LIMIT 50")->asArray()->All();
        return $this->renderAjax('refresh-person',['person'=>$person]);
    }
    
    public function actionRefreshLocalisation(){
        $localisation = OeuvreLocalisation::findBySql("SELECT * FROM oeuvre_localisation ORDER BY nom_localisation ASC LIMIT 50")->asArray()->All(); 
        return $this->renderAjax('refresh-localisation',['localisation'=>$localisation]);
    }
    
    public function actionSearchKeyword($search){
        $keywords = NoticesKeywords::findBySql("SELECT * FROM notices_keywords WHERE mots_cles LIKE '%$search%' ORDER BY mots_cles")->asArray()->all(); 
        return $this->renderAjax('search-keyword',['keywords'=>$keywords]);
    }
    
    public function actionSearchPerson($search){
        $person = AuthorsOeuvre::findBySql("SELECT * FROM authors_oeuvre WHERE index_person LIKE '%$search%' ORDER BY nom")->asArray()->All();
        return $this->renderAjax('search-person',['person'=>$person]);
    }
    
    public function actionSearchLocalisation($search){
        $localisation = OeuvreLocalisation::findBySql("SELECT * FROM oeuvre_localisation WHERE nom_localisation LIKE '%$search%' ORDER BY nom_localisation")->asArray()->All(); 
        return $this->renderAjax('search-localisation',['localisation'=>$localisation]);
    }
    
    public function actionUpdateOeuvre(){
        if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
                $kote_pou_modifye = $_POST['name']; 
                switch ($kote_pou_modifye){
                    case 'code':
                    {
                        $notices = \app\models\Notices::findOne($_POST['pk']); 
                        $notices->code = $_POST['value']; 
                        $notices->save();
                    }
                    break;
                    case 'signature':
                    {
                        $notices = Notices::findOne($_POST['pk']); 
                        $notices->signature = $_POST['value'];
                        $notices->save();
                    }
                    break;
                    case 'ancien_no_inventaire':
                    {
                        $notice_id = $_POST['pk'];
                        $notices_custom = NoticesCustomValues::findOne(['notices_custom_champ'=>5,'notices_custom_origine'=>$notice_id]); 
                        $notices_custom->notices_custom_small_text = $_POST['value']; 
                        $notices_custom->save(); 
                    }
                    break;
                    case 'artiste':
                    {
                        $responsability = Responsability::findOne($_POST['pk']); 
                        $responsability->responsability_author = $_POST['value']; 
                        $responsability->save(); 
                    }
                    break;
                    case 'titre' :
                    {
                        $notices = Notices::findOne($_POST['pk']); 
                        $notices->tit1 = $_POST['value'];
                        $notices->save(); 
                    }
                    break;
                    case 'titre_provisoire':
                    {
                        $notices = Notices::findOne($_POST['pk']); 
                        $notices->tit2 = $_POST['value'];
                        $notices->save(); 
                    }
                    break;
                    case 'date_creation':
                    {
                        $notices = Notices::findOne($_POST['pk']); 
                        $notices->date_parution = date("Y-m-d H:i:s",strtotime($_POST['value']));
                        $notices->save();
                    }
                    break;
                    case 'lieu_creation':
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->lieu_creation = $_POST['value']; 
                        $notices_other_data->save(); 
                    }
                    break; 
                    case 'longueur': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->longue = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break; 
                    case 'largeur': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->larg = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break; 
                    case 'hauteur': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->haut = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break; 
                    case 'materiaux': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->materiaux = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break; 
                    case 'technique': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->technique = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break;
                    case 'description': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->description = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break;
                    case 'categorie':
                    {
                        $notices = Notices::findOne($_POST['pk']); 
                        $notices->typdoc = $_POST['value'];
                        $notices->save(); 
                    }
                    break;
                    case 'motscles':
                    {
                        $oeuvres_motscles = OeuvreMotcles::findOne($_POST['pk']); 
                        $oeuvres_motscles->mots_cles = $_POST['value'];
                        $oeuvres_motscles->save(); 
                    }
                    break;
                    case 'legende_image': 
                    {
                        $notices_other_data = NoticesOtherData::findOne($_POST['pk']); 
                        $notices_other_data->legende_image = $_POST['value']; 
                        $notices_other_data->save();
                    }
                    break;
                    case 'statut':
                    {
                        $notice_id = $_POST['pk'];
                        $notices_custom = NoticesCustomValues::findOne(['notices_custom_champ'=>1,'notices_custom_origine'=>$notice_id]); 
                        $notices_custom->notices_custom_small_text = $_POST['value']; 
                        $notices_custom->save(); 
                    }
                    break;
                
                    case 'type_acquisition':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->type_acquisition = $_POST['value']; 
                        $oeuvre_provenance->save(); 
                    }
                    
                    break;
                    case 'personne_acquisition':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->person_acquisition = $_POST['value']; 
                        $oeuvre_provenance->save(); 
                    }
                    break;
                    case 'date_acquisition':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->date_acquisition = date("Y-m-d H:i:s",strtotime($_POST['value']));
                        $oeuvre_provenance->save(); 
                    }
                    break;
                    case 'prix_achat':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->prix_achat = $_POST['value'];
                        $oeuvre_provenance->save(); 
                    }
                    break;
                    case 'valeur_assurance':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->valeur_assurance = $_POST['value'];
                        $oeuvre_provenance->save(); 
                    }
                    break;
                    case 'vente':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->vente = $_POST['value'];
                        $oeuvre_provenance->save(); 
                    }
                    
                    break;
                    case 'date_vente':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->date_vente = date("Y-m-d H:i:s",strtotime($_POST['value']));
                        $oeuvre_provenance->save(); 
                    }
                    break;
                     case 'acheteur':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->acheteur = $_POST['value'];
                        $oeuvre_provenance->save(); 
                    }
                    break;
                    case 'prix_vente':
                    {
                        
                        $oeuvre_provenance = OeuvreProvenance::findOne($_POST['pk']);
                        $oeuvre_provenance->prix_vente = $_POST['value'];
                        $oeuvre_provenance->save(); 
                    }
                    
                    break;
                    case 'localisation':
                    {
                        
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->localisation = $_POST['value'];
                        $oeuvre_physique->save(); 
                    }
                    break;
                    case 'emplacement':
                    {
                        
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->emplacement = $_POST['value'];
                        $oeuvre_physique->save(); 
                    }
                    break;
                    case 'etat_conservation':
                    {
                        
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->etat_conservation = $_POST['value'];
                        $oeuvre_physique->save(); 
                    }
                    break;
                    case 'constat':
                    {
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->constat = $_POST['value'];
                        $oeuvre_physique->save(); 
                    }
                    break;
                    case 'nom_restaurateur':
                    {
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->nom_restaurateur = $_POST['value'];
                        $oeuvre_physique->save(); 
                    }
                    break;
                    case 'date_restauration':
                    {
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->date_restauration = date("Y-m-d h:i:s",strtotime($_POST['value']));
                        $oeuvre_physique->save(); 
                    }
                    break;
                    case 'object_restauration':
                    {
                        $oeuvre_physique = OeuvrePhysique::findOne($_POST['pk']);
                        $oeuvre_physique->object_restauration = $_POST['value'];
                        $oeuvre_physique->save(); 
                    }
                    
                    
                    
                }
                // POST data is valid.
                
            }  
    }
    
    public function  actionListArtiste(){
         $all_authors = Authors::findBySql("SELECT author_id AS 'value', CONCAT(author_name,' ',author_rejete) AS 'text'  FROM authors ORDER BY author_name")->asArray()->all();
         $json_authors =json_encode($all_authors,false); 
         return $json_authors; 
    }
    
    public function  actionListLocalisation(){
         $all_localisation = OeuvreLocalisation::findBySql("SELECT nom_localisation AS 'value', nom_localisation AS 'text' FROM oeuvre_localisation ORDER BY nom_localisation")->asArray()->all();
         $json_localisation =json_encode($all_localisation,false); 
         return $json_localisation; 
    }
    
    public function  actionListAuthorsOeuvre(){
         $all_authors = Authors::findBySql("SELECT id AS 'value', CONCAT(nom,' ',prenom) AS 'text'  FROM authors_oeuvre ORDER BY nom")->asArray()->all();
         $json_authors =json_encode($all_authors,false); 
         return $json_authors; 
    }
    
    public function actionListMateriaux(){
        $all_materiaux = [
            ['value'=>'Materiau 1', 'text'=>'Materiau 1'],
            ['value'=>'Materiau 2', 'text'=>'Materiau 2'],
            ['value'=>'Materiau 3', 'text'=>'Materiau 3'],
            ['value'=>'Materiau 4', 'text'=>'Materiau 4'],
            ]; 
        $json_materiaux =json_encode($all_materiaux,false); 
         return $json_materiaux;
    }
    
    public function actionListEtatConservation(){
        $all_materiaux = [
            ['value'=>'A+', 'text'=>'A+'],
            ['value'=>'A-', 'text'=>'A-'],
            ['value'=>'B+', 'text'=>'B+'],
            ['value'=>'B-', 'text'=>'B-'],
            ['value'=>'C', 'text'=>'C'],
            ]; 
        $json_materiaux =json_encode($all_materiaux,false); 
         return $json_materiaux;
    }
    
    public function actionListTechnique(){
        $all_technique = [
            ['value'=>'Technique 1', 'text'=>'Technique 1'],
            ['value'=>'Technique 2', 'text'=>'Technique 2'],
            ['value'=>'Technique 3', 'text'=>'Technique 3'],
            ['value'=>'Technique 4', 'text'=>'Technique 4'],
            ]; 
        $json_technique =json_encode($all_technique,false); 
         return $json_technique;
    }
    
    public function actionListCategorie(){
        $all_categorie = [
            ['value'=>'o', 'text'=>'Sculpture'],
            ['value'=>'p', 'text'=>'Art graphique'],
            ['value'=>'q', 'text'=>'Objet décoratif'],
            ['value'=>'s', 'text'=>'Photographie'],
            ]; 
        $json_categorie =json_encode($all_categorie,false); 
         return $json_categorie;
    }
    
    public function actionListStatut(){
        $all_statut = [
            ['value'=>'Collection permanente', 'text'=>'Collection permanente'],
            ['value'=>'Consignation', 'text'=>'Consignation'],
            ['value'=>'Propriété Antonio JOSEPH', 'text'=>'Propriété Antonio JOSEPH'],
            ['value'=>'Consultation sur place', 'text'=>'Consultation sur place'],
            ]; 
        $json_statut =json_encode($all_statut,false); 
         return $json_statut;
    }

    public function actionListTypeAcquisition(){
        $all_acquisition = [
            ['value'=>'Don', 'text'=>'Don'],
            ['value'=>'Achat', 'text'=>'Achat'],
            ['value'=>'Legs', 'text'=>'Legs'],
            
            ]; 
        $json_acquisition =json_encode($all_acquisition,false); 
         return $json_acquisition;
    }

    public function actionDeletecarte($id){
        $carte = LogipamCarteMembre::findOne($id);
        $file_name = $carte->image_name;
        $file_to_delete = '../../../../photos/lecteurs/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'.$file_to_delete.'"');
        $carte->delete(); 
        
    }
    
    public function actionDeletekat($id){
        $sql = "SELECT * FROM logipam_carte_membre WHERE empr_cb = '$id'"; 
        $carte = LogipamCarteMembre::findBySql($sql)->all();
        $file_name = $carte->image_name;
        $id_kat = $carte->id;
        $carte1 = LogipamCarteMembre::findOne($id_kat);
        $file_to_delete = '../../../../photos/lecteurs/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'.$file_to_delete.'"');
        $carte1->delete(); 
        
    }
    
    // for jpg 
    /**
     * 
     * @param type $file
     * @param type $w
     * @param type $h
     * @return type
     */
public function resize_imagejpg($file, $w, $h) {
   list($width, $height) = getimagesize($file);
   $src = imagecreatefromjpeg($file);
   $dst = imagecreatetruecolor($w, $h);
   imagecopyresampled($dst, $src, 0, 0, 0, 0, $w, $h, $width, $height);
   return $dst;
}

public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
}