<?php
use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
  
     
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2">
            <ul class="sidebar-menu tree" data-widget="tree">
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-calendar"></i>
                    <span>Collection</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: block;">
                    <li id="oeuvre"><a href="#"><i class="fa fa-eye"></i> Oeuvre d'art</a></li>
                    <!--<li id="search"><a href="#"><i class="fa fa-search"></i> Rechercher</a></li> -->
                    <li id="pret"><a href="#"><i class="fa fa-cart-plus"></i> Pr&ecirc;ts et expositions</a></li>
                    <li id="retour"><a href="#"><i class="fa fa-arrow-down"></i> Retour</a></li>
                    <li id="list"><a href="#"><i class="fa fa-list"></i> Gerer les listes</a></li>
                  </ul>
                </li>
                
                
            </ul>
           
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10"> <?=  $content ?></div>
        
    </div>

</html>
<?php $this->endPage() ?>        
    
  
