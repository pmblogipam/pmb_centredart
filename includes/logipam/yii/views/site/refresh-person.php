<table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $l = 1;
                    foreach($person as $pe){
                        ?>
                <tr>
                    <td><?= $l; ?></td>
                    <td class="modifye" data-type="text" data-name="prenom" data-pk="<?= $pe['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $pe['prenom'] ?>
                    </td>
                    <td class="modifye" data-type="text" data-name="nom" data-pk="<?= $pe['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $pe['nom']; ?>
                    </td>
                    <td><span class="fa fa-trash efase-person" data-idperson="<?= $pe['id']; ?>"></span></td>
                </tr>
                <?php 
                    
                    $l++;
                    }
                ?>
            </tbody>
                
</table>

<script>
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'Aucune information',
          
            
        });
    });
</script>