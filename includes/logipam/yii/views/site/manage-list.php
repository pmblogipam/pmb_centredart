<?php 
if (! isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
}
?>
<form action="includes/logipam/yii/web/index.php?r=site/update-oeuvre" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<div class="row-fluid">
    <p>
        Gestion des listes ouvertes 
    </p>
    
</div>
<div class="row-fluid">
    <div class="col-xs-4">
        <div class="input-group">
            <input class="form-control" id="search-keyword" name="search-keyword" type="text" placeholder="Rechercher mots clés"> 
           <span class="input-group-btn"> 
                <button type="button" class="btn btn-primary" id="add-keyword">
                    <i class="fa fa-plus"></i>
                    </button> 
            </span>
                                
        </div>
        <table class="table table-striped" id="tablo-motscles">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $k = 1;
                    foreach($keywords as $key){
                        ?>
                <tr>
                    <td><?= $k; ?></td>
                    <td class="modifye" data-type="text" data-name="keywords" data-pk="<?= $key['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $key['mots_cles']; ?>
                    </td>
                    <td><span class="fa fa-trash efase-keyword" data-idkey="<?= $key['id']; ?>"></span></td>
                </tr>
                <?php 
                    
                    $k++;
                    }
                ?>
            </tbody>
                
        </table>
        
    </div>
    <div class="col-xs-4">
        <div class="input-group">
            <input class="form-control" id="search-person" name="search-person" type="text" placeholder="Rechercher personne"> 
           <span class="input-group-btn"> 
                <button type="button" class="btn btn-primary" id="add-person">
                    <i class="fa fa-plus"></i>
                    </button> 
            </span>
                                
        </div>
        <div id="tablo-person">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $l = 1;
                    foreach($person as $pe){
                        ?>
                <tr>
                    <td><?= $l; ?></td>
                    <td class="modifye" data-type="text" data-name="prenom" data-pk="<?= $pe['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $pe['prenom'] ?>
                    </td>
                    <td class="modifye" data-type="text" data-name="nom" data-pk="<?= $pe['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $pe['nom']; ?>
                    </td>
                    <td><span class="fa fa-trash efase-person" data-idperson="<?= $pe['id']; ?>"></span></td>
                </tr>
                <?php 
                    
                    $l++;
                    }
                ?>
            </tbody>
                
        </table>
        </div>
    </div>
    <div class="col-xs-4">
        
        <div class="input-group">
            <input class="form-control" id="search-localisation" name="search-localisation" type="text" placeholder="Rechercher localisation"> 
           <span class="input-group-btn"> 
                <button type="button" class="btn btn-primary" id="add-localisation">
                    <i class="fa fa-plus"></i>
                    </button> 
            </span>
                                
        </div>
        <table class="table table-striped" id="tablo-localisation">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $m = 1;
                    foreach($localisation as $lo){
                        ?>
                <tr>
                    <td><?= $m; ?></td>
                    <td class="modifye" data-type="text" data-name="localisation" data-pk="<?= $lo['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $lo['nom_localisation']; ?>
                    </td>
                    <td><span class="fa fa-trash efase-localisation" data-idlocalisation="<?= $lo['id']; ?>"></span></td>
                </tr>
                <?php 
                    
                    $m++;
                    }
                ?>
            </tbody>
                
        </table>
    </div>
</div>
</form>

 <!-- Modal pour ajouter les mots cles -->
 
 <!-- Modal -->
  <div class="modal fade" id="modal-mots-cles" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter mots cl&eacute;s</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="key-word">Mots cl&eacute;s</label>
                            <input type="text" class="form-control" id="key-word" name="key-word">
                    </div>
                </div>
                
            </div>
            
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-mots-cles' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter une personne  -->
 <div class="modal fade" id="modal-person" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter personne </h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="prenom-person">Pr&eacute;nom personne</label>
                            <input type="text" class="form-control" id="prenom-person" name="prenom-person">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-person">Nom personne</label>
                            <input type="text" class="form-control" id="nom-person" name="nom-person">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="date-person">Date</label>
                            <input type="text" class="form-control" id="date-person" name="date-person">
                    </div>
                </div>
                
            </div>
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-person' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter localisation --> 
 
  <div class="modal fade" id="modal-localisation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter localisation</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="nom-localisation">Nom localisation </label>
                            <input type="text" class="form-control" id="nom-localisation" name="nom-localisation">
                    </div>
                </div>
                
            </div>
            
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-localisation' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>

<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'Aucune information',
          
            
        });
        
        // Pour gerer l'ajout des mots cles 
        // Faire afficher le modal d'ajout des mots cles
        $('#add-keyword').click(function(){
            $("#modal-mots-cles").modal();
            $('#modal-mots-cles').on('hidden.bs.modal', function () {
               
              });
        });
        // Enregistrer les mots cles 
        $('#save-mots-cles').click(function(){
            var key_word = $('#key-word').val();
            //alert(key_word);
            $.get('includes/logipam/yii/web/index.php?r=site/add-keywords',{mots:key_word}, function(data){
                $.get('includes/logipam/yii/web/index.php?r=site/refresh-keyword',{},function(data){
                     $('#tablo-motscles').html(data);
                });
             
              $('#key-word').val('');
              $('#modal-mots-cles').modal('toggle');
            });
            
        });
        
        // Gerer l'ajout de personne 
            
       // Faire afficher modal personne 
        $('#add-person').click(function(){
            $("#modal-person").modal();
            $('#modal-person').on('hidden.bs.modal', function () {
               $('#prenom-person').val('');
               $('#nom-person').val('');
               $('#date-person').val('');
              });
        });
        
        // Enregistrer personne lie a l'acquisition dans la table authors_oeuvre 
        $('#save-person').click(function(){
            var prenom_person = $('#prenom-person').val();
            var nom_person = $('#nom-person').val();
            var date_person = $('#date-person').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-author-oeuvre',{
                prenom:prenom_person, 
                nom:nom_person,
                date:date_person,
                type: "1"
            }, function(data){
               $.get('includes/logipam/yii/web/index.php?r=site/refresh-person',{},function(data){
                     $('#tablo-person').html(data);
                });
              $('#prenom-person').val('');
              $('#nom-person').val('');
              $('#date-person').val('');
              $('#modal-person').modal('toggle');
            });
            
        });
        
       // Add localisation 
       // Faire afficher le modal d'ajout de localisation
        $('#add-localisation').click(function(){
            $("#modal-localisation").modal();
            $('#modal-localisation').on('hidden.bs.modal', function () {
               
              });
        });
        
        // Enregistrer les localisations 
        $('#save-localisation').click(function(){
            var nom_localisation = $('#nom-localisation').val();
            //alert(key_word);
            $.get('includes/logipam/yii/web/index.php?r=site/add-localisation',{nom_localisation:nom_localisation}, function(data){
                $.get('includes/logipam/yii/web/index.php?r=site/refresh-localisation',{},function(data){
                    $('#tablo-localisation').html(data);
                });
                
                $('#nom-localisation').val('');
                $('#modal-localisation').modal('toggle');
            });
            
        });
        
        $('#search-keyword').keyup(function(){
           var keyword = $('#search-keyword').val();
            $.get('includes/logipam/yii/web/index.php?r=site/search-keyword',{search:keyword},function(data){
                    $('#tablo-motscles').html(data);
                });
        });
        
        $('#search-person').keyup(function(){
           var person = $('#search-person').val();
            $.get('includes/logipam/yii/web/index.php?r=site/search-person',{search:person},function(data){
                    $('#tablo-person').html(data);
                });
        });
        
        $('#search-localisation').keyup(function(){
           var localisation = $('#search-localisation').val();
            $.get('includes/logipam/yii/web/index.php?r=site/search-localisation',{search:localisation},function(data){
                    $('#tablo-localisation').html(data);
                });
        });
        
        // Delete keywords 
        $('.efase-keyword').click(function(){
            var result = confirm("Voulez vous vraiment supprimer ce mot clé ?");
            if(result){
            var keyword_id = $(this).data().idkey;
               
                $.get('includes/logipam/yii/web/index.php?r=site/delete-keyword',{id:keyword_id},function(data){
                    $.get('includes/logipam/yii/web/index.php?r=site/refresh-keyword',{},function(data){
                         $('#tablo-motscles').html(data);
                    });
                });
            }
        });
        
        // Delete person 
         
        $('.efase-person').click(function(){
            var result = confirm("Voulez vous vraiment supprimer cette personne ?");
            if(result){
            var keyword_id = $(this).data().idperson;
               
                $.get('includes/logipam/yii/web/index.php?r=site/delete-person',{id:keyword_id},function(data){
                    $.get('includes/logipam/yii/web/index.php?r=site/refresh-person',{},function(data){
                         $('#tablo-person').html(data);
                    });
                });
            }
        });
        
        // Delete localisation
        $('.efase-localisation').click(function(){
            var result = confirm("Voulez vous vraiment supprimer cette localisation ?");
            if(result){
            var keyword_id = $(this).data().idlocalisation;
               
                $.get('includes/logipam/yii/web/index.php?r=site/delete-localisation',{id:keyword_id},function(data){
                    $.get('includes/logipam/yii/web/index.php?r=site/refresh-localisation',{},function(data){
                         $('#tablo-localisation').html(data);
                    });
                });
            }
        });
        
       
    });
</script>