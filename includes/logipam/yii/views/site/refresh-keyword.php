<table class="table table-striped" id="tablo-motscles">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $k = 1;
                    foreach($keywords as $key){
                        ?>
                <tr>
                    <td><?= $k; ?></td>
                    <td class="modifye" data-type="text" data-name="keywords" data-pk="<?= $key['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $key['mots_cles']; ?>
                    </td>
                    <td><span class="fa fa-trash efase-keyword" data-idkey="<?= $key['id']; ?>"></span></td>
                </tr>
                <?php 
                    
                    $k++;
                    }
                ?>
            </tbody>
                
</table>

<script>
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'Aucune information',
          
            
        });
    });
</script>