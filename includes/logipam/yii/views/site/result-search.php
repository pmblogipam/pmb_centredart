<?php 
$cat_array = ['t'=>'Peinture','o'=>'Sculpture','p'=>'Art graphique','q'=>'Objet décoratif','s'=>'Photographie',''=>'Pas de catégorie'];

?>
<div>
    <p> </p><p> </p>
    
    <?php if(!empty($all_oeuvre)) { ?>
    
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <th>No inventaire</th>
        <th>Titre</th>
        <th>Artiste</th>
        <th>Catégorie</th>
        </thead>
        <tbody>
            <?php 
                for($i=0;$i<count($all_oeuvre); $i++){
                    ?>
                <tr class="notice-id" data-noticeid="<?=$all_oeuvre[$i]['notice_id']?>">
         
                    <td><a a="href=#"><?= $all_oeuvre[$i]['code']?></a></td>
                    <td><?= $all_oeuvre[$i]['tit1']?></td>
                    <td><?= $all_oeuvre[$i]['nom_auteur']?></td>
                    <td><?= $cat_array[$all_oeuvre[$i]['typdoc']]?></td>
        
               <tr>
                <?php 
                    }
                ?>
            </ul>
        </table>
    <?php } else { ?>
        Aucune oeuvre trouvée avec cette recherche 
    <?php } ?>
    
</div>

<script>
    $(document).ready(function(){
        
        $('.notice-id').click(function(){
            var notice_id = $(this).data().noticeid;
            $.get('includes/logipam/yii/web/index.php?r=site/view-oeuvre',{notice_id:notice_id},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-last-oeuvre').hide();
            $('#place-search-oeuvre').hide();
            $('#place-view-oeuvre').show(); 
            $('#place-view-oeuvre').html(data);
            });
        });
        
       
       
    });
</script>