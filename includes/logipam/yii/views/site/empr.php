<?php 
use app\models\LogipamPresence; 

$sql_code = "SELECT id_empr, empr_cb, empr_nom,empr_tel1, empr_mail, statut_libelle, empr_prenom, empr_year, empr_ville, empr_sexe, empr_date_adhesion, empr_date_expiration, last_loan_date FROM empr "
        . " INNER JOIN empr_statut ON (empr_statut = idstatut) WHERE empr_cb = '$code' "; 
       $liste_lecteur = \Yii::$app->getDb()->createCommand($sql_code)->queryAll();
       
       // Calcul du mombre de pret en cours
       $id_empr = $liste_lecteur[0]['id_empr']; 
       
       $sql_pret = "SELECT count(*) as total_pret FROM pret WHERE pret_idempr = $id_empr";
       
      $total_pret = \Yii::$app->getDb()->createCommand($sql_pret)->queryAll();
      
      // Verifier que le lecteur n'est pas encore enregistre present durant un jour 
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $nombre_presence_lecteur = LogipamPresence::find()->andWhere(['=','date_presence',$today])->andWhere(['=','id_empr',$id_empr])->count();  
        
        
        
        
   // if(isset($id_empr)){    
        
     if($nombre_presence_lecteur == 0){
      
   
      // Enregistrer la presence 
      $presence = new LogipamPresence(); 
      $presence->id_empr = $liste_lecteur[0]['id_empr'];
      $presence->empr_cb = $liste_lecteur[0]['empr_cb'];
      $presence->date_presence = date('Y-m-d'); 
      $presence->time_presence = date('H:i:s');
      $presence->day_presence = date('w');
      $presence->save(); 
      
    ?>     
       
<div id='el11Parent' class='notice-parent search-result-item first-child'>
  
   		<h1 id='empr-name' class='uk-flex uk-flex-between'><div class='left'>
           <?php
            if($liste_lecteur[0]['empr_date_expiration']<=$today){
                
                ?>
                        <div class="alert alert-danger">Attention la carte de ce lecteur est p&eacute;rim&eacute;e depuis le <strong><?= Yii::$app->formatter->asDate($liste_lecteur[0]['empr_date_expiration']) ?></strong> </div>
           <?php 
            }
           ?>             
           <span class='empr-name h3-like'><?= $liste_lecteur[0]['empr_prenom']." ".$liste_lecteur[0]['empr_nom']?> </span> <font size='2'><i class='fa fa-minus' aria-hidden='true'></i>Pr&ecirc;t(s): <?= $total_pret[0]['total_pret'] ?> </font></div><div class='right uk-clearfix'><font class='uk-label ui-label-custom' size='2'><?=$liste_lecteur[0]['statut_libelle'] ?></font></div></h1>
</div>

<div id="bloc_adresse_empr" class="uk-flex uk-flex-between wyr-flex-container">
	<div class="colonne4 wyr-flex-item">
		
		<div class="row uk-clearfix">
                    <strong>T&eacute;l. :</strong> <strong><?= $liste_lecteur[0]['empr_tel1'] ?></strong>
		</div>
		<div class="row uk-clearfix">
			<strong>E-mail : </strong> <a href="mailto:<?= $liste_lecteur[0]['empr_mail'] ?>"><?= $liste_lecteur[0]['empr_mail'] ?></a>
                </div>
                <div class="row uk-clearfix">
		<strong>Adh&eacute;sion</strong>
                </div>
                <div class="row uk-clearfix">
                    <strong>D&eacute;but : </strong><?= Yii::$app->formatter->asDate($liste_lecteur[0]['empr_date_adhesion']); ?>
                        </div>
                <div class="row uk-clearfix">
                        <strong>Fin : </strong><?= Yii::$app->formatter->asDate($liste_lecteur[0]['empr_date_expiration']); ?>
                </div>
                
	</div>
	<div class="colonne4 wyr-flex-item">
		
		<div class="row uk-clearfix">
			<strong>Ann&eacute;e de naissance : </strong><?= $liste_lecteur[0]['empr_year']; ?>
                </div>
		<div class="row uk-clearfix">
			<strong>Sexe : </strong><?= $liste_lecteur[0]['empr_sexe']==1 ? 'Masculin':'F&eacute;minin'  ?>
                </div>
                <div class="row uk-clearfix">
		<strong>Ville : </strong><?= $liste_lecteur[0]['empr_ville']; ?>
                </div>

                <div class="row uk-clearfix">
                        <strong>Num&eacute;ro : </strong><?= $liste_lecteur[0]['empr_cb'] ?>
                </div>
                <div class="row uk-clearfix">
                    <strong>
                        <?= "Arriv&eacute; le: ".Yii::$app->formatter->asDate($presence->date_presence)." &agrave; ".Yii::$app->formatter->asTime($presence->time_presence)?>
                    </strong>    
                </div>
                
	</div>
    <div class="colonne4 wyr-flex-item">
    <img  src='photos/lecteurs/<?= $liste_lecteur[0]['empr_cb']?>.JPG'  height="200" width="200"/>
   </div>
	


<div class="row uk-clearfix uk-flex uk-flex-between wyr-flex-container wyr-flex-item">
</div>
<div class="row wyr-flex-item"></div>
<div class="colonne3 wyr-flex-item full-width"><div class="row uk-clearfix"><b>Commentaire : </b></div></div>
<div class="colonne3 wyr-flex-item full-width"><div class="row uk-clearfix">
		<strong>Dernier emprunt : </strong><?= Yii::$app->formatter->asDate($liste_lecteur[0]['last_loan_date']); ?>
    </div></div>
<div class="row wyr-flex-item"></div>
</div>
<?php 

     } else{
         
         ?>
<div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i> Attention!</h4>
                Le lecteur <strong>"<?= $liste_lecteur[0]['empr_prenom']." ".$liste_lecteur[0]['empr_nom'] ?>"</strong> a &eacute;t&eacute; d&eacute;j&agrave; enregistr&eacute; comme pr&eacute;sent aujourd'hui !
</div>

<?php 


     }
     
