<?php 
    if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
?>
<form id='frm-pret-expo'>
     <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#pret-expo-1" aria-expanded="true">Pr&ecirc;ts/Expositions</a></li>
            <li class=""><a data-toggle="tab" href="#pret-expo-2" aria-expanded="false">Emprunteur</a></li>
             <li class=""><a data-toggle="tab" href="#pret-expo-3" aria-expanded="false">Mus&eacute;e</a></li>
        </ul>
        <div class="tab-content">
            <div id="pret-expo-1" class="tab-pane active">
                <div class="panel-body">
                <div class="col-xs-4">
                   <div class="input-group">
                       <input class="form-control" id="search-pret" name="search-pret" type="text" placeholder="Rechercher pr&ecirc;t"> 
                       <span class="input-group-btn"> 
                            <button type="button" class="btn btn-primary" id="add-pret">
                                <i class="fa fa-plus"></i>
                                </button> 
                        </span>
                    </div>
                </div>
                </div>
            </div>
            <div id="pret-expo-2" class="tab-pane">
                <div class="panel-body">
                    <div class="col-xs-4">
                   <div class="input-group">
                       <input class="form-control" id="search-emprunteur" name="search-emprunteur" type="text" placeholder="Rechercher emprunteur"> 
                       <span class="input-group-btn"> 
                            <button type="button" class="btn btn-primary" id="add-emprunteur">
                                <i class="fa fa-plus"></i>
                                </button> 
                        </span>
                    </div>
                </div>
                </div>
            </div>
            <div id="pret-expo-3" class="tab-pane">
                <div class="panel-body">
                    <div class="col-xs-4">
                   <div class="input-group">
                       <input class="form-control" id="search-musee" name="search-musee" type="text" placeholder="Rechercher mus&eacute;e"> 
                       <span class="input-group-btn"> 
                            <button type="button" class="btn btn-primary" id="add-musee">
                                <i class="fa fa-plus"></i>
                                </button> 
                        </span>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
    </div>
</form>

<!-- Modal de creation des prets -->
 <div class="modal fade lg" id="modal-create-pret" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter pr&ecirc;t</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-12">
                    <div class="input-group">
                            <input type="text" class="form-control" id="no-inventaire-pret" name="no-inventaire-pret">
                            <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-oeuvre-pret">
                                        <i class="fa fa-arrow-down"></i>
                                </button> 
                            </span>
                            <input type='hidden' id='no-inv-selected'>
                    </div>
                </div>
            </div>
            <div class="row" id="en-tete-pret">
                <div class="col-xs-12">
                    <h4>Liste des oeuvres &agrave; pr&ecirc;ter (<span id="konte-pret"></span>)</h4>
                </div>
            </div>
            <div id="oeuvre-pret">
                
            </div>
            
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="musee">Nom mus&eacute;e</label>
                            <input type="text" class="form-control" id="musee" name="musee">
                            <input type='hidden' id='id-musee'>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="titre-expo">Titre de l'exposition</label>
                            <input type="text" class="form-control" id="titre-expo" name="titre-expo">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="date-pret">Date pr&ecirc;t</label>
                            <input type="text" class="form-control" id="date-pret" name="date-pret">
                    </div>
                </div>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="date-retour-prog">Date retour programm&eacute;e</label>
                            <input type="text" class="form-control" id="date-retour-prog" name="date-retour-prog">
                    </div>
                </div>
            </div>
            
                
         </div>
            
        <div class="modal-footer">
            <div class='row'>
                <div class='col-xs-2'>
                    
                </div>
                <div class='col-xs-4'>
                     <div class="form-group">
                        <button type="button" id='save-pret' class='btn btn-success'>Enregistrer</button>
                    </div> 
                </div>
                <div class='col-xs-4'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
                <div class='col-xs-2'>
                    
                </div>
            </div>
          
          
        </div>
           </div>
      </div>
      
    </div>
 
<!-- Modal de creation des emprunteurs -->
 <div class="modal fade lg" id="modal-create-emprunteur" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter emprunteur</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="prenom-emprunteur">Pr&eacute;nom</label>
                            <input type="text" class="form-control" id="prenom-emprunteur" name="prenom-emprunteur">
                   </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-emprunteur">Nom</label>
                            <input type="text" class="form-control" id="nom-emprunteur" name="nom-emprunteur">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="phone-emprunteur">T&eacute;l&eacute;phone</label>
                            <input type="text" class="form-control" id="phone-emprunteur" name="phone-emprunteur">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="adresse-emprunteur">Adresse emprunteur</label>
                            <input type="text" class="form-control" id="adresse-emprunteur" name="adresse-emprunteur">
                    </div>
                </div>
            </div>
           </div>
            
        <div class="modal-footer">
            <div class='row'>
                <div class='col-xs-2'>
                    
                </div>
                <div class='col-xs-4'>
                     <div class="form-group">
                        <button type="button" id='save-emprunteur' class='btn btn-success'>Enregistrer</button>
                    </div> 
                </div>
                <div class='col-xs-4'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
                <div class='col-xs-2'>
                    
                </div>
            </div>
          
          
        </div>
           </div>
      </div>
      
    </div>

<!-- Modal de creation des musees -->
 <div class="modal fade lg" id="modal-create-musee" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter mus&eacute;e</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-musee">Nom musée</label>
                            <input type="text" class="form-control" id="nom-musee" name="nom-musee">
                   </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="adresse-musee">Adresse</label>
                            <input type="text" class="form-control" id="adresse-musee" name="adresse-musee">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="phone-musee">T&eacute;l&eacute;phone</label>
                            <input type="text" class="form-control" id="phone-musee" name="phone-musee">
                    </div>
                </div>
                
            </div>
           </div>
            
        <div class="modal-footer">
            <div class='row'>
                <div class='col-xs-2'>
                    
                </div>
                <div class='col-xs-4'>
                     <div class="form-group">
                        <button type="button" id='save-musee' class='btn btn-success'>Enregistrer</button>
                    </div> 
                </div>
                <div class='col-xs-4'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
                <div class='col-xs-2'>
                    
                </div>
            </div>
          
          
        </div>
           </div>
      </div>
      
    </div>



<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    
    
    
    $(document).ready(function(){
        $('#en-tete-pret').hide();
        var konte = 0;
        
        $('#add-oeuvre-pret').click(function(){
            $('#en-tete-pret').show();
            var no_inventaire_pret = $('#no-inventaire-pret').val();
            var no_inv_selected = $('#no-inv-selected').val();
            $('#oeuvre-pret').append('<div class="alert alert-success alert-dismissible femen-pret"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> '+no_inventaire_pret+'</div>');
            $('#oeuvre-pret').append('<input id="id-notice-'+konte+'" value="'+no_inv_selected+'" type="hidden">');
            $('#no-inventaire-pret').val('');
            konte++;
            $('#konte-pret').html(konte);
           
        });
        
        $(document).on('click','.femen-pret',function(){
                konte--;
               $('#konte-pret').html(konte);
              $('#id-notice-'+konte).remove();
        });
        
        
        $('#date-pret').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        $('#date-retour-prog').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
         // Faire afficher modal pret 
        $('#add-pret').click(function(){
            $("#modal-create-pret").modal();
            $('#modal-create-pret').on('hidden.bs.modal', function () {
               //$('#prenom-artiste').val('');
               //$('#nom-artiste').val('');
               //$('#date-artiste').val('');
              });
        });
        
        
        
         // Faire afficher modal emprunteur 
        $('#add-emprunteur').click(function(){
            $("#modal-create-emprunteur").modal();
            $('#modal-create-emprunteur').on('hidden.bs.modal', function () {
               //$('#prenom-artiste').val('');
               //$('#nom-artiste').val('');
               //$('#date-artiste').val('');
              });
        });
        
        // Enregistrer emprunteur dans la table collection_emprunteur 
        $('#save-emprunteur').click(function(){
            var nom_emprunteur = $('#nom-emprunteur').val();
            var prenom_emprunteur = $('#prenom-emprunteur').val();
            var phone_emprunteur = $('#phone-emprunteur').val();
            var adresse_emprunteur = $('#adresse-emprunteur').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-emprunteur',{nom_empr:nom_emprunteur, prenom_empr:prenom_emprunteur,phone_empr:phone_emprunteur,adresse_empr:adresse_emprunteur}, function(data){
              // $('#artiste-selected').val(data); 
              // $('#artiste').val(nom_artiste+' '+prenom_artiste); 
              $('#nom-emprunteur').val('');
              $('#prenom-emprunteur').val('');
              $('#phone-emprunteur').val('');
              $('#adresse-emprunteur').val('');
              $('#modal-create-emprunteur').modal('toggle');
            });
            
        });
        
         // Faire afficher modal musee
        $('#add-musee').click(function(){
            $("#modal-create-musee").modal();
            $('#modal-create-musee').on('hidden.bs.modal', function () {
               //$('#prenom-artiste').val('');
               //$('#nom-artiste').val('');
               //$('#date-artiste').val('');
              });
        });
        
        // Enregistrer musee dans la table collection_musee 
        $('#save-musee').click(function(){
            var nom_musee = $('#nom-musee').val();
            var adresse_musee = $('#adresse-musee').val();
            var phone_musee = $('#phone-musee').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-musee',{nom_musee:nom_musee, adresse_musee:adresse_musee,phone_musee:phone_musee}, function(data){
              // $('#artiste-selected').val(data); 
              // $('#artiste').val(nom_artiste+' '+prenom_artiste); 
              $('#nom-musee').val('');
              $('#adresse-musee').val('');
              $('#phone-musee').val('');
              $('#modal-create-musee').modal('toggle');
            });
            
        });
        
        
        // Preparer la list deroulante des oeuvres 
        $('#no-inventaire-pret').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = <?= $json_notices; ?> // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#no-inv-selected').val(map[item].id);
                return item;
            }
                
                
            });
        
    });
</script>