<p>
    
</p>
<p>
    
</p>
<form id="search-form">
<div class='row'>
    
    <div class='row'>
       <div class="col-xs-8"> 
        <div class="input-group">
            <input placeholder="Rechercher dans tous les champs" name="btn-search" class="form-control input-lg" type="text">
            <div class="input-group-btn">
                <button id="btn-search" class="btn btn-lg btn-warning" type="reset">
                    Rechercher
                </button>
            </div>
       </div>
       </div>
        
        
        <div class="col-xs-2">
        </div>
         
    </div>
    
</div>
    
</form>

<div class="row" id="result-search">
    <div class="row">
        <table class="table table-striped">
            <tr>
                
                <td colspan="2">
                    <strong>Description scientifique</strong>
                </td>
                
            </tr>
            <tr>
                <td>
                    No inventaire
                </td>
                <td>CA1-PrD037</td>
            </tr>
            <tr>
                <td>Ancien No inventaire</td>
                <td> D884 PAC03 CRCH/CA-2010-001-0213</td>
            </tr>
            <tr>
                <td>Artiste</td>
                <td>Préfète DUFFAUT (1922-2012) </td>
            </tr>
            <tr>
                <td>Titre</td>
                <td>3eme Tentation</td>
            </tr>
            <tr>
                <td>Titre provisoire</td>
                <td></td>
            </tr>
            <tr>
                <td>Date cr&eacute;ation</td>
                <td>2003</td>
            </tr>
            <tr>
                <td>Lieu de cr&eacute;ation</td>
                <td>Jacmel</td>
                <td></td>
            </tr>
            <tr>
                <td>Mat&eacute;riaux et techniques</td>
                <td>Huile sur masonite</td>
            </tr>
            <tr>
                <td>Dimension (in)</td>
                <td></td>
            </tr>
            <tr>
                <td>Dimension (mm)</td>
                <td></td>
            </tr>
            <tr>
                <td>Cat&eacute;gorie</td>
                <td><a href="#">Peinture</a></td>
            </tr>
            <tr>
                <td>Description</td>
                <td>Scene de combat, deux chiens se battent, des assistant, des maisons, des arbres</td>
            </tr>
            <tr>
                <td>Expositions</td>
                <td><a href="#">musée, titre de l’expo, dates</a></td>
            </tr>
            <tr>
                <td>Mots clés</td>
                <td><a>Combat, animal, pariage</a></td>
            </tr>
            <tr>
                <th scope="row">Conditions d’acquisition</th>
            </tr>
            <tr>
                <td>Statut</td>
                <td><a>Collection permanente</a></td>
            </tr>
            <tr>
                <td>Provenance</td>
                <td>nom, date, type d’acquistion (don/ achat)</td>
            </tr>
            <tr>
                <td>Prix d'achat</td>
                <td></td>
            </tr>
            <tr>
                <td>Valeur d’assurance</td>
                <td></td>
            </tr>
            <tr>
                <td>Vente</td>
                <td>nom, date, prix</td>
            </tr>
            <tr>
                <th scope="row">Conditions d’acquisition</th>
            </tr>
            <tr>
                <td>Localisation</td>
                <td>Centre d'art</td>
            </tr>
            <tr>
                <td>Etat de conservation</td>
                <td>A - : toile fendue côté gauche, quelques décolements de peinture</td>
            </tr>
            <tr>
                <td>Restauration effectuée</td>
                <td>Date, nom, actions, matériaux</td>
            </tr>
            <tr>
                <td>Pret</td>
                <td>Musée, titre de l’expo, dates</td>
            </tr>
        </table>
    </div>
</div>



<script>
$(document).ready(function(){
    $("#result-search").hide(); 
    
    $("#btn-search").click(function(){
        $("#result-search").show(); 
    });
}); 


</script>