<?php 
use app\models\Notices; 
use app\models\NoticesCustomValues; 
use app\models\Responsability; 
use yii\widgets\ActiveForm;
use app\models\NoticesOtherData; 
use app\models\OeuvreMotcles;
use app\models\OeuvreProvenance;
use app\models\AuthorsOeuvre; 
use app\models\OeuvrePhysique; 
// Create a new CSRF token.

if (! isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
}

// Definir les variables 
$no_inventaire = null; 
$ancien_no_inventaire  = null;
$categorie  = null;
$titre = null;
$signature  = null;
$titre_provisoire = null;
$date_creation = null;
$statut = null;
$lieu_creation = null;
$longue = null;
$larg = null;
$haut = null;
$materiaux = null; 
$technique = null;
$description = null;
$legende_image = null;
$artiste = null;
$mots_cles = null;
$type_acquisition = null;
$person_acquisition = null;
$date_acquisition = null;
$prix_achat = null;
$valeur_assurance = null;
$vente = null;
$date_vente = null;
$acheteur = null;
$prix_vente = null;
$localisation = null;
$emplacement = null;
$etat_conservation = null;
$constat = null;
$nom_restaurateur = null;
$date_restauration = null;
$object_restauration  = null;

// Autres data 
$id_custom = null;
$id_responsabilty = null;
$id_notices_other_data = null; 
$cat_array = ['t'=>'Peinture','o'=>'Sculpture','p'=>'Art graphique','q'=>'Objet décoratif','s'=>'Photographie',''=>'Pas de catégorie'];
$id_oeuvre_motscles = null;
$authors_oeuvre = new AuthorsOeuvre(); 
$id_oeuvre_provenance = null;
$id_oeuvre_physique = null; 

$all_notices = Notices::findBySql("SELECT * FROM notices WHERE notice_id = $notice_id")->asArray()->all();
$all_notices_custom = NoticesCustomValues::findBySql("SELECT * FROM notices_custom_values WHERE notices_custom_origine = $notice_id")->asArray()->all();
$all_authors = Responsability::findBySql("SELECT id_responsability, responsability_author, responsability_notice, author_name, author_rejete FROM `responsability` r JOIN authors a ON (a.author_id = r.responsability_author) WHERE responsability_notice = $notice_id")->asArray()->all();
$all_notice_other_data = NoticesOtherData::findBySql("SELECT * FROM notices_other_data WHERE notices_id = $notice_id")->asArray()->all();
$all_oeuvre_mots_cles = OeuvreMotcles::findBySql("SELECT * FROM oeuvre_motcles WHERE notice_id = $notice_id")->asArray()->all();
$all_oeuvre_provenance = OeuvreProvenance::findBySql("SELECT * FROM oeuvre_provenance  WHERE notice_id = $notice_id")->asArray()->all(); 
$all_oeuvre_physique = OeuvrePhysique::findBySql("SELECT * FROM `oeuvre_physique` where notice_id = $notice_id")->asArray()->all(); 

if(!empty($all_notices)){
    $no_inventaire = $all_notices[0]['code']; 
    $signature = $all_notices[0]['signature'];
    $titre = $all_notices[0]['tit1'];
    $titre_provisoire = $all_notices[0]['tit2'];
    $date_creation = $all_notices[0]['date_parution'];
    $categorie = $all_notices[0]['typdoc'];
    
}

if(!empty($all_notices_custom)){
    $ancien_no_inventaire = $all_notices_custom[0]['notices_custom_small_text'];
    $statut = $all_notices_custom[1]['notices_custom_small_text'];
}

if(!empty($all_authors)){
    $artiste = $all_authors[0]['author_rejete'].' '.$all_authors[0]['author_name'];
    $id_responsability = $all_authors[0]['id_responsability']; 
}

if(!empty($all_notice_other_data)){
    $lieu_creation = $all_notice_other_data[0]['lieu_creation'];
    $id_notices_other_data = $all_notice_other_data[0]['id'];
    $longue = $all_notice_other_data[0]['longue'];
    $larg = $all_notice_other_data[0]['larg'];
    $haut = $all_notice_other_data[0]['haut'];
    $materiaux = $all_notice_other_data[0]['materiaux'];
    $technique = $all_notice_other_data[0]['technique'];
    $description = $all_notice_other_data[0]['description'];
    $legende_image = $all_notice_other_data[0]['legende_image'];
}

if(!empty($all_oeuvre_mots_cles)){
    $mots_cles = $all_oeuvre_mots_cles[0]['mots_cles'];
    $id_oeuvre_motscles = $all_oeuvre_mots_cles[0]['id'];
}

if(!empty($all_oeuvre_provenance)){
    $type_acquisition = $all_oeuvre_provenance[0]['type_acquisition']; 
    $person_acquisition = $all_oeuvre_provenance[0]['person_acquisition']; 
    $id_oeuvre_provenance = $all_oeuvre_provenance[0]['id'];
    $date_acquisition = $all_oeuvre_provenance[0]['date_acquisition'];
    $prix_achat = $all_oeuvre_provenance[0]['prix_achat'];
    $valeur_assurance = $all_oeuvre_provenance[0]['valeur_assurance'];
    $vente = $all_oeuvre_provenance[0]['vente'];
    $date_vente = $all_oeuvre_provenance[0]['date_vente'];
    $acheteur = $all_oeuvre_provenance[0]['acheteur'];
    $prix_vente = $all_oeuvre_provenance[0]['prix_vente'];    
}

if(!empty($all_oeuvre_physique)){
    $id_oeuvre_physique = $all_oeuvre_physique[0]['id'];
    $localisation = $all_oeuvre_physique[0]['localisation']; 
    $emplacement = $all_oeuvre_physique[0]['emplacement']; 
    $etat_conservation = $all_oeuvre_physique[0]['etat_conservation']; 
    $constat = $all_oeuvre_physique[0]['constat']; 
    $nom_restaurateur = $all_oeuvre_physique[0]['nom_restaurateur']; 
    $date_restauration = $all_oeuvre_physique[0]['date_restauration'];
    $object_restauration = $all_oeuvre_physique[0]['object_restauration'];
    
}

?>


<form action="includes/logipam/yii/web/index.php?r=site/update-oeuvre" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<div class="tabs-container">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#view-1" aria-expanded="true">Description Scientifique</a></li>
        <li class=""><a data-toggle="tab" href="#view-2" aria-expanded="false">Provenance</a></li>
        <li class=""><a data-toggle="tab" href="#view-3" aria-expanded="false">Vente</a></li>
        <li class=""><a data-toggle="tab" href="#view-4" aria-expanded="false">Etat Physique</a></li>
     </ul>
    <div class="tab-content">
        <div id="view-1" class="tab-pane active">
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td>
                                <strong>Numéro inventaire</strong>
                            </td>
                            
                            <td class="modifye" data-type="text" data-name="code" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre">
                                <?= $no_inventaire; ?>
                            </td>
                           
                            <td>
                                <strong>Ancien numéro inventaire </strong>
                            </td>
                             <td class="modifye" data-type="text" data-name="ancien_no_inventaire" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre">
                               <?= $ancien_no_inventaire; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Signature</strong>
                            </td>
                            <td class="modifye" data-type="text" data-name="signature" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                                <?= $signature ?>
                            </td>
                            <td>
                                <strong>Artiste</strong>
                            </td>
                            <td class="modifye" data-title="Choisir artiste" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-artiste" data-name="artiste" data-pk="<?= $id_responsability; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                             <?= $artiste ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Titre</strong></td>
                            <td class="modifye" data-type="text" data-name="titre" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $titre ?>
                            </td>
                            <td><strong>Titre provisoire</strong></td>
                            <td class="modifye" data-type="text" data-name="titre_provisoire" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $titre_provisoire ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Date création</strong></td>
                            <td class="modifye" data-type="date" data-name="date_creation" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= Yii::$app->formatter->asDate($date_creation); ?>
                            </td>
                            <td><strong>Lieu de création</strong></td>
                            <td class="modifye" data-type="text" data-name="lieu_creation" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $lieu_creation; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Dimension</strong></td>
                            <td colspan="3">
                                <strong>Longueur : </strong> <span class="modifye" data-type="text" data-name="longueur" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" ><?= $longue ?></span> Pouces (<?= $longue*2.54?> cm)
                                <strong>Largeur : </strong> <span class="modifye" data-type="text" data-name="largeur" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre"><?= $larg ?></span> Pouces (<?= $larg*2.54?> cm)
                                <strong>Hauteur : </strong> <span class="modifye" data-type="text" data-name="hauteur" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre"> <?= $haut ?></span> Pouces (<?= $haut*2.54?> cm)
                            </td>
                            
                        </tr>
                        <tr>
                            <td><strong>Matériaux</strong></td>
                            <td class="modifye" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-materiaux" data-name="materiaux" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $materiaux; ?>
                            </td>
                            <td><strong>Technique</strong></td>
                            <td class="modifye" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-technique" data-name="technique" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $technique; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Description</strong></td>
                            <td class="modifye" data-type="text"  data-name="description" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $description; ?>
                            </td>
                            <td><strong>Categorie</strong></td>
                            <td class="modifye" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-categorie" data-name="categorie" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $cat_array[$categorie]; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Mots clés</strong></td>
                            <td class="modifye" data-type="text"  data-name="motscles" data-pk="<?= $id_oeuvre_motscles; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $mots_cles; ?>
                            </td>
                            <td><strong>Légende image</strong></td>
                            <td class="modifye" data-type="text"  data-name="legende_image" data-pk="<?= $id_notices_other_data; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $legende_image; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Statut</strong></td>
                            <td class="modifye" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-statut" data-name="statut" data-pk="<?= $notice_id; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $statut ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div id="view-2" class="tab-pane">
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td><strong>Type d'acquisition</strong></td>
                            <td class="modifye" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-type-acquisition" data-name="type_acquisition" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $type_acquisition; ?>
                            </td>
                            <td><strong>Personne liée à l'acquisition </strong></td>
                            <td class="modifye" data-type="select" data-source="includes/logipam/yii/web/index.php?r=site/list-authors-oeuvre" data-name="personne_acquisition" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $authors_oeuvre->getFullName($person_acquisition); ?>
                            </td>
                            
                        </tr>
                        <tr>
                            <td><strong>Date acquisition</strong></td>
                            <td class="modifye" data-type="date"  data-name="date_acquisition" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= Yii::$app->formatter->asDate($date_acquisition); ?>
                            </td>
                            <td><strong>Prix achat</strong></td>
                            <td class="modifye" data-type="text"  data-name="prix_achat" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $prix_achat; ?>
                            </td>
                            
                        </tr>
                        <tr>
                            <td><strong>Valeur assurance</strong></td>
                            <td class="modifye" data-type="text"  data-name="valeur_assurance" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $valeur_assurance ?>
                            </td>
                        </tr>
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
         <div id="view-3" class="tab-pane">
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover">
                     <tbody>
                         <tr>
                             <td><strong>Date vente</strong></td>
                            <td class="modifye" data-type="date"  data-name="date_vente" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= Yii::$app->formatter->asDate($date_vente); ?>
                            </td> 
                            <td><strong>Acheteur</strong></td>
                            <td class="modifye" data-type="select"  data-source="includes/logipam/yii/web/index.php?r=site/list-authors-oeuvre" data-name="acheteur" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $authors_oeuvre->getFullName($acheteur); ?>
                            </td>
                         </tr>
                         <tr>
                            <td><strong>Prix de vente</strong></td>
                            <td class="modifye" data-type="text"  data-name="prix_vente" data-pk="<?= $id_oeuvre_provenance; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $prix_vente; ?>
                            </td>
                        </tr>
                     </tbody>
                </table>
            </div>
         </div>
        <div id="view-4" class="tab-pane">
            <div class="panel-body">
                 <table class="table table-striped table-bordered table-hover">
                     <tbody>
                         <tr>
                             <td><strong>Localisation</strong></td>
                            <td class="modifye" data-type="select"  data-source="includes/logipam/yii/web/index.php?r=site/list-localisation" data-name="localisation" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $localisation; ?>
                            </td>
                            <td><strong>Emplacement</strong></td>
                            <td class="modifye" data-type="text"   data-name="emplacement" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $emplacement; ?>
                            </td>
                         </tr>
                         <tr>
                             <td><strong>Etat de conservation</strong></td>
                            <td class="modifye" data-type="select"  data-source="includes/logipam/yii/web/index.php?r=site/list-etat-conservation" data-name="etat_conservation" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $etat_conservation; ?>
                            </td>
                            <td><strong>Constat</strong></td>
                            <td class="modifye" data-type="text"   data-name="constat" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $constat; ?>
                            </td>
                         </tr>
                         <tr>
                             <td><strong>Nom restaurateur</strong></td>
                            <td class="modifye" data-type="select"  data-source="includes/logipam/yii/web/index.php?r=site/list-authors-oeuvre" data-name="nom_restaurateur" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $authors_oeuvre->getFullName($nom_restaurateur); ?>
                            </td>
                            <td><strong>Date restauration</strong></td>
                            <td class="modifye" data-type="date"   data-name="date_restauration" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= Yii::$app->formatter->asDate($date_restauration); ?>
                            </td>
                         </tr>
                         <tr>
                             <td><strong>Objet restauration</strong></td>
                            <td class="modifye" data-type="text" data-name="object_restauration" data-pk="<?= $id_oeuvre_physique; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-oeuvre" >
                               <?= $object_restauration; ?>
                            </td>
                            
                         </tr>
                     </tbody>
                 </table>
            </div>
        </div>
        
    </div>
</div>

</form>

<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    
    $(document).ready(function(){
        
        $('.modifye').editable({
          emptytext:'Aucune information',
          
            
        });
        
        
    });
</script>
