<?php
use app\models\LogipamPresence; 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$sql = "SELECT  lp.empr_cb, e.empr_prenom, e.empr_nom, IF(e.empr_sexe=1,'Masculin','Feminin') AS sexe,e.empr_date_adhesion, e.empr_date_expiration, lp.date_presence FROM logipam_presence lp INNER JOIN empr e ON (e.id_empr = lp.id_empr) WHERE lp.date_presence = date(NOW()) ORDER BY e.empr_nom";

$data_presence_today = LogipamPresence::findBySql($sql)->asArray()->all(); 


?>

<div class='row'>
    <div class='col-lg-5 col-xs-5'>
        <div class="form-group">
                <label>Date d&eacute;but</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control pull-right" id="date_start" type="text">
                </div>
                <!-- /.input group -->
        </div>
    </div>
    <div class='col-lg-5 col-xs-5'>
        <div class="form-group">
                <label>Date Fin:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control pull-right" id="date_end" type="text">
                </div>
                <!-- /.input group -->
        </div>
    </div>
    <div class='col-lg-2 col-xs-2'>
        <div class="form-group">
            <label> &thinsp;</label>

                <div class="input-group">
                  
                    <button class='btn btn-info' id='btnShow'>Afficher</button>
                </div>
                <!-- /.input group -->
        </div>
    </div>
    
</div>
<div class='row'>
    <div id='liste_defo'>
        <table id='tablo-defo' class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Pr&eacute;nom</th>
                    <th>Sexe</th>
                    <th>Code lecteur</th>
                    <th>Date pr&eacutesence</th>
                    <th>Adh&eacute;sion</th>
                    <th>Expiration</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for($i=0;$i<sizeof($data_presence_today);$i++){
                        ?>
                <tr>
                    <td><?=$data_presence_today[$i]['empr_nom'] ?></td>
                    <td><?=$data_presence_today[$i]['empr_prenom'] ?></td>
                    <td><?=$data_presence_today[$i]['sexe'] ?></td>
                    <td><?=$data_presence_today[$i]['empr_cb'] ?></td>
                    <td><?= Yii::$app->formatter->asDate($data_presence_today[$i]['date_presence']) ?></td>
                    <td><?= Yii::$app->formatter->asDate($data_presence_today[$i]['empr_date_adhesion']) ?></td>
                    <td><?= Yii::$app->formatter->asDate($data_presence_today[$i]['empr_date_expiration']) ?></td>
                </tr>
            <?php 
                        
                    }
                ?>
            </tbody>
        </table>
    </div>
    <div id='list_param'></div>
    
</div>
<script type="text/javascript">
   $(document).ready(function(){
        $('#date_start').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        $('#date_end').datepicker({
           autoclose: true
        });
        
        
        // Affiche le tableau parametre
        $("#btnShow").click(function(){
            var dateStart = $("#date_start").datepicker("getDate"); // get the date object
            var dateStartString = dateStart.getFullYear() + '-' + (dateStart.getMonth() + 1) + '-' + dateStart.getDate();
            var dateEnd = $("#date_end").datepicker("getDate"); // get the date object
            var dateEndString = dateEnd.getFullYear() + '-' + (dateEnd.getMonth() + 1) + '-' + dateEnd.getDate();
            //alert(dateEndString);
            $('#liste_defo').empty();
            $.get('includes/logipam/yii/web/index.php?r=site/get-raport-param',{date1 : dateStartString, date2 : dateEndString},function(data){
            $('#list_param').html(data);
            
            });
            totalVisite(); 
            totalFemaleVisite();
            totalMaleVisite();
            adhesionDepassee(); 
            $("#empr_cb").val("");
            $("#empr_cb").focus();
        });
        
        
    });
    
    
    $(document).ready(function(){
            $('#tablo-defo').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    //lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [

                 {extend: 'excel', title: 'liste_presence', text: '<i class="fa fa-external-link"> Exporter vers Excel</i>'},
                 
                   // {extend: 'pdf', title: 'ExampleFile'},

                    
                ]

            });

        });

</script>