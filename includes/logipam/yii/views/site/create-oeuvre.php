<form id='notice'>
    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">Description Scientifique</a></li>
            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Provenance</a></li>
            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Vente</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false">Etat Physique</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="no-inventaire">Num&eacute;ro inventaire <span id="error-no-inventaire" style="color: #FF0000;"></span></label>
                                    <input type="text" class="form-control" id="no-inventaire" name="no-inventaire">
                                    
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="ancien-no-inventaire">Ancien num&eacute;ro inventaire</label>
                                    <input type="text" class="form-control" id="ancien-no-inventaire" name="ancien-no-inventaire">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="signature">Signature</label>
                                    <input type="text" class="form-control" id="signature" name="signature">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <label for="artiste">Artiste <span id="error-artiste" style="color: #FF0000;"></span></label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="artiste" name="artiste" data-selectedid=""> 
                                <!--  Place the id of the artiste here -->
                                <input type="hidden" id="artiste-selected"  value=""/>
                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-artiste">
                                        <i class="fa fa-plus"></i>
                                        </button> 
                                </span>
                                
                            </div>
                            
                            
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="titre">Titre <span id="error-titre" style="color: #FF0000;"></span></label>
                                    <input type="text" class="form-control" id="titre" name="titre">
                                    
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="titre">Titre provisoire</label>
                                    <input type="text" class="form-control" id="titre-provisoire" name="titre-provisoire">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="date-creation">Année cr&eacute;ation</label>
                                    <input type="text" class="form-control" id="date-creation" name="date-creation">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="lieu-creation">Lieu cr&eacute;ation</label>
                                    <input type="text" class="form-control" id="lieu-creation" name="lieu-creation">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="dim-long">Longueur</label>
                                    <input type="text" class="form-control" id="dim-long" name="dim-long">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="dim-larg">Largeur</label>
                                    <input type="text" class="form-control" id="dim-larg" name="dim-larg">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="dim-haut">Hauteur</label>
                                    <input type="text" class="form-control" id="dim-haut" name="dim-haut">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="materiaux">Mat&eacute;riaux</label>
                                <select class="form-control" id="materiaux" name="materiaux">
                                    <option value="mat1">Materiau 1</option>
                                    <option value="mat2">Materiau 2</option>
                                    <option value="mat3">Materiau 3</option>
                                    <option value="mat4">Materiau 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="technique">Technique</label>
                                <select class="form-control" id="technique" name="technique">
                                    <option value="tech1">Technique 1</option>
                                    <option value="tech2">Technique 2</option>
                                    <option value="tech3">Technique 3</option>
                                    <option value="tech4">Technique 4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" class="form-control" id="description" name="description">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="categorie">Catégorie</label>
                                <select class="form-control" id="categorie" name="categorie">
                                    <option value="t">Peinture</option>
                                    <option value="o">Sculpture</option>
                                    <option value="p">Art graphique</option>
                                    <option value="q">Objet décoratif</option>
                                    <option value="s">Photographie</option>
                                    <option value="f">Fer découpé</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="mots-cles">Mots cl&eacute;s</label>
                                <div class="input-group">
                                <input type="text" class="form-control" id="mots-cles" name="mots-cles">
                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-mots-cles">
                                        <i class="fa fa-plus"></i>
                                        </button> 
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="legende">L&eacute;gende de l'image</label>
                                <input type="text" class="form-control" id="legende" name="legende">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="statut">Statut</label>
                                <select class="form-control" id="statut" name="statut">
                                    <option value="Collection permanente">Collection permanente</option>
                                    <option value="Consignation">Consignation</option>
                                    <option value="A définir">A définir</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="type-acquisition">Type d'acquisition</label>
                                <select class="form-control" id="type-acquisition" name="type-acquisition">
                                    <option value="Don">Don</option>
                                    <option value="Achat">Achat</option>
                                    <option value="Legs">Legs</option>
                                    
                                </select>
                                </div>
                        </div>
                        <div class="col-xs-6">
                            <label for="person-acquisition">Personne li&eacute;e &agrave; l'acquisition </label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="person-acquisition" name="person-acquisition">
                                <input type="hidden" id="person-acquisition-selected" name="person-acquisition-selected">
                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-person-acquisition">
                                        <i class="fa fa-plus"></i>
                                        </button> 
                                </span>
                            </div>
                        </div>
                        
                    </div>
                    <div class='row'>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="date-acquisition">Date acquisition</label>
                                    <input type="text" class="form-control" id="date-acquisition" name="date-acquisition">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="prix-achat">Prix d'achat</label>
                                    <input type="text" class="form-control" id="prix-achat" name="prix-achat">
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="valeur-assurance">Valeur d'assurance</label>
                                    <input type="text" class="form-control" id="valeur-assurance" name="valeur-assurance">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            
                        </div>
                    </div>
                    
                    
                        
                   </div>
            </div>
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">
                     
                          <div class='row'>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="date-vente">Date de vente</label>
                                    <input type="text" class="form-control" id="date-vente" name="date-vente">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <label for="acheteur">Acheteur </label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="acheteur" name="acheteur">
                                <input type='hidden' id="acheteur-selected" name="acheteur-selected">
                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-acheteur">
                                        <i class="fa fa-plus"></i>
                                        </button> 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="prix-vente">Prix de vente</label>
                                    <input type="text" class="form-control" id="prix-vente" name="prix-vente">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            
                        </div>
                    </div>
                     
                </div>
            </div>
            <div id="tab-4" class="tab-pane">
                <div class="panel-body">
                    <div class='row'>
                        <div class="col-xs-6">
                            <label for="localisation">Localisation </label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="localisation" name="localisation"> 
                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-localisation">
                                        <i class="fa fa-plus"></i>
                                        </button> 
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="emplacement">Emplacement</label>
                                    <input type="text" class="form-control" id="emplacement" name="emplacement">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="etat-conservation">Etat de conservation</label>
                                <select class="form-control" id="etat-conservation" name="etat-conservation">
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="C">C</option>
                                </select>
                                </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="constat">Constat</label>
                                    <input type="text" class="form-control" id="constat" name="constat">
                            </div>
                        </div>
                        
                    </div>
                    <div class='row'>
                        <div class="col-xs-6">
                            <label for="restaurateur">Nom du restaurateur </label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="restaurateur" name="restaurateur"> 
                                <input type='hidden' id="restaurateur-selected" name="restaurateur-selected"> 
                                <span class="input-group-btn"> 
                                    <button type="button" class="btn btn-primary" id="add-restaurateur">
                                        <i class="fa fa-plus"></i>
                                        </button> 
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="date-restauration">Date de la restauration</label>
                                    <input type="text" class="form-control" id="date-restauration" name="date-restauration">
                            </div>
                        </div>
                        
                    </div>
                    <div class='row'>
                        
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="object-restauration">Object de la restauration</label>
                                    <input type="text" class="form-control" id="object-restauration" name="object-restauration">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            
                        </div>
                        
                    </div>

                    </div>
            </div>
        </div>

    </div>
    
  <div class="row">
      <div class="col-xs-3">
                            
      </div>
      <div class="col-xs-6">
           <a class="btn btn-lg btn-success" name="save-oeuvre" id="save-oeuvre">Enregistrer</a> 
           <a class="btn btn-lg btn-warning" name="cancel" id="save-oeuvre">Anuuler</a> 
      </div>
        
      <div class="col-xs-3">
                            
      </div>
  </div>
   
</form>
 

<div id="result">
    
</div>
<!-- Modal pour ajouter les artistes -->
 
 <!-- Modal -->
  <div class="modal fade" id="modal-artiste" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter artiste</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="prenom-artiste">Pr&eacute;nom artiste</label>
                            <input type="text" class="form-control" id="prenom-artiste" name="prenom-artiste">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-artiste">Nom artiste</label>
                            <input type="text" class="form-control" id="nom-artiste" name="nom-artiste">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="date-artiste">Date</label>
                            <input type="text" class="form-control" id="date-artiste" name="date-artiste">
                    </div>
                </div>
                
            </div>
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-artiste' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter les mots cles -->
 
 <!-- Modal -->
  <div class="modal fade" id="modal-mots-cles" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter mots cl&eacute;s</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="key-word">Mots cl&eacute;s</label>
                            <input type="text" class="form-control" id="key-word" name="key-word">
                    </div>
                </div>
                
            </div>
            
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-mots-cles' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter une personne lie a l'acquisition -->
 <div class="modal fade" id="modal-person" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter personne li&eacute;e &agrave; l'acquisition de l'oeuvre</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="prenom-person">Pr&eacute;nom personne</label>
                            <input type="text" class="form-control" id="prenom-person" name="prenom-person">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-person">Nom personne</label>
                            <input type="text" class="form-control" id="nom-person" name="nom-person">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="date-person">Date</label>
                            <input type="text" class="form-control" id="date-person" name="date-person">
                    </div>
                </div>
                
            </div>
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-person' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter un acheteur -->
  <div class="modal fade" id="modal-acheteur" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter acheteur</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="prenom-acheteur">Pr&eacute;nom acheteur</label>
                            <input type="text" class="form-control" id="prenom-acheteur" name="prenom-acheteur">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-acheteur">Nom acheteur</label>
                            <input type="text" class="form-control" id="nom-acheteur" name="nom-acheteur">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="date-acheteur">Date</label>
                            <input type="text" class="form-control" id="date-acheteur" name="date-acheteur">
                    </div>
                </div>
                
            </div>
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-acheteur' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter restaurateur -->
  <div class="modal fade" id="modal-restaurateur" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter restaurateur</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="prenom-restaurateur">Pr&eacute;nom restaurateur</label>
                            <input type="text" class="form-control" id="prenom-restaurateur" name="prenom-restaurateur">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-restaurateur">Nom restaurateur</label>
                            <input type="text" class="form-control" id="nom-restaurateur" name="nom-restaurateur">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="date-restaurateur">Date</label>
                            <input type="text" class="form-control" id="date-restaurateur" name="date-restaurateur">
                    </div>
                </div>
                
            </div>
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-restaurateur' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Modal pour ajouter localisation --> 
 
  <div class="modal fade" id="modal-localisation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter localisation</h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="nom-localisation">Nom localisation </label>
                            <input type="text" class="form-control" id="nom-localisation" name="nom-localisation">
                    </div>
                </div>
                
            </div>
            
            <div class='row'>
              <div class="col-xs-2">
              </div>
              <div class="col-xs-12">
                  <div class="form-group">
                    <button type="button" id='save-localisation' class='btn btn-success'>Enregistrer</button>
                  </div>
              </div>
              <div class="col-xs-2">
              </div>
          </div>
            
        </div>
          
        <div class="modal-footer">
            
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
      </div>
      
    </div>
  </div>
 
 
 <script>
     $(document).ready(function(){
        
        $('#error-no-inventaire').hide();
        $('#error-artiste').hide(); 
        $('#error-titre').hide();
        // Liste de choix des artistes 
           $('#artiste').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = <?= $json_authors; ?> // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#artiste-selected').val(map[item].id);
                return item;
            }
                
                
            });
         
         // Faire afficher modal artiste 
        $('#add-artiste').click(function(){
            $("#modal-artiste").modal();
            $('#modal-artiste').on('hidden.bs.modal', function () {
               $('#prenom-artiste').val('');
               $('#nom-artiste').val('');
               $('#date-artiste').val('');
              });
        });
        
        // Enregistrer artiste dans la table authors 
        $('#save-artiste').click(function(){
            var prenom_artiste = $('#prenom-artiste').val();
            var nom_artiste = $('#nom-artiste').val();
            var date_artiste = $('#date-artiste').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-artiste',{prenom:prenom_artiste, nom:nom_artiste,date:date_artiste}, function(data){
              $('#artiste-selected').val(data); 
              $('#artiste').val(nom_artiste+' '+prenom_artiste); 
              $('#prenom-artiste').val('');
              $('#nom-artiste').val('');
              $('#date-artiste').val('');
              $('#modal-artiste').modal('toggle');
            });
            
        });
        
        // Faire afficher le modal d'ajout des mots cles
        $('#add-mots-cles').click(function(){
            $("#modal-mots-cles").modal();
            $('#modal-mots-cles').on('hidden.bs.modal', function () {
               
              });
        });
        // Enregistrer les mots cles 
        $('#save-mots-cles').click(function(){
            var key_word = $('#key-word').val();
            //alert(key_word);
            $.get('includes/logipam/yii/web/index.php?r=site/add-keywords',{mots:key_word}, function(data){
               // alert(data);
              var un_mot = data; 
              $("#mots-cles").val(function() {
                    return this.value + un_mot+',';
                });
             // $('#mots-cles').append(data); 
              $('#key-word').val('');
              $('#modal-mots-cles').modal('toggle');
            });
            
        });
        
        // Enregistrer les localisations 
        $('#save-localisation').click(function(){
            var nom_localisation = $('#nom-localisation').val();
            //alert(key_word);
            $.get('includes/logipam/yii/web/index.php?r=site/add-localisation',{nom_localisation:nom_localisation}, function(data){
                $('#localisation').val(data);
                $('#nom-localisation').val('');
                $('#modal-localisation').modal('toggle');
            });
            
        });
        
         // Liste de choix des localisations  
           $('#localisation').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = <?= $json_localisation; ?> // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#person-acquisition-selected').val(map[item].id);
                return item;
            }
                
                
            });
            
        
        
         // Liste de choix des personnes liees a l'acquition  
           $('#person-acquisition').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = <?= $json_authors_oeuvre; ?> // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#person-acquisition-selected').val(map[item].id);
                return item;
            }
                
                
            });
            
             // Faire afficher modal personne 
        $('#add-person-acquisition').click(function(){
            $("#modal-person").modal();
            $('#modal-person').on('hidden.bs.modal', function () {
               $('#prenom-person').val('');
               $('#nom-person').val('');
               $('#date-person').val('');
              });
        });
        
        // Enregistrer personne lie a l'acquisition dans la table authors_oeuvre 
        $('#save-person').click(function(){
            var prenom_person = $('#prenom-person').val();
            var nom_person = $('#nom-person').val();
            var date_person = $('#date-person').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-author-oeuvre',{
                prenom:prenom_person, 
                nom:nom_person,
                date:date_person,
                type: "1"
            }, function(data){
              $('#person-acquisition-selected').val(data); 
              $('#person-acquisition').val(nom_person+' '+prenom_person); 
              $('#prenom-person').val('');
              $('#nom-person').val('');
              $('#date-person').val('');
              $('#modal-person').modal('toggle');
            });
            
        });
        
        // Liste de choix des acheteurs  
           $('#acheteur').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = <?= $json_authors_oeuvre; ?> // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#acheteur-selected').val(map[item].id);
                return item;
            }
                
                
            });
            
             // Faire afficher modal acheteur
        $('#add-acheteur').click(function(){
            $("#modal-acheteur").modal();
            $('#modal-acheteur').on('hidden.bs.modal', function () {
               $('#prenom-acheteur').val('');
               $('#nom-acheteur').val('');
               $('#date-acheteur').val('');
              });
        });
        
        // Enregistrer acheteur dans la table authors_oeuvre 
        $('#save-acheteur').click(function(){
            var prenom_acheteur = $('#prenom-acheteur').val();
            var nom_acheteur = $('#nom-acheteur').val();
            var date_acheteur = $('#date-acheteur').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-author-oeuvre',{
                prenom:prenom_acheteur, 
                nom:nom_acheteur,
                date:date_acheteur,
                type: "2"
            }, function(data){
              $('#acheteur-selected').val(data); 
              $('#acheteur').val(nom_acheteur+' '+prenom_acheteur); 
              $('#prenom-acheteur').val('');
              $('#nom-acheteur').val('');
              $('#date-acheteur').val('');
              $('#modal-acheteur').modal('toggle');
            });
            
        });
        
        // Liste de choix des restaurateur  
           $('#restaurateur').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = <?= $json_authors_oeuvre; ?> // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#restaurateur-selected').val(map[item].id);
                return item;
            }
                
                
            });
            
             // Faire afficher modal acheteur
        $('#add-restaurateur').click(function(){
            $("#modal-restaurateur").modal();
            $('#modal-restaurateur').on('hidden.bs.modal', function () {
               $('#prenom-restaurateur').val('');
               $('#nom-restaurateur').val('');
               $('#date-restaurateur').val('');
              });
        });
        
        // Enregistrer acheteur dans la table authors_oeuvre 
        $('#save-restaurateur').click(function(){
            var prenom_restaurateur = $('#prenom-restaurateur').val();
            var nom_restaurateur = $('#nom-restaurateur').val();
            var date_restaurateur = $('#date-restaurateur').val();
            $.get('includes/logipam/yii/web/index.php?r=site/add-author-oeuvre',{
                prenom:prenom_restaurateur, 
                nom:nom_restaurateur,
                date:date_restaurateur,
                type: "3"
            }, function(data){
              $('#restaurateur-selected').val(data); 
              $('#restaurateur').val(nom_restaurateur+' '+prenom_restaurateur); 
              $('#prenom-restaurateur').val('');
              $('#nom-restaurateur').val('');
              $('#date-restaurateur').val('');
              $('#modal-restaurateur').modal('toggle');
            });
            
        });
        
        // Faire afficher le modal d'ajout de localisation
        $('#add-localisation').click(function(){
            $("#modal-localisation").modal();
            $('#modal-localisation').on('hidden.bs.modal', function () {
               
              });
        });
        
        $('#no-inventaire').keyup(function(){
            $('#no-inventaire').removeClass("ere-koule");
            $('#error-no-inventaire').hide(); 
        });
        
        $('#artiste').keyup(function(){
            $('#artiste').removeClass("ere-koule");
            $('#error-artiste').hide(); 
        });
        
        $('#titre').keyup(function(){
            $('#titre').removeClass("ere-koule");
            $('#error-titre').hide(); 
        });
        
        $('#artiste').change(function(){
            $('#artiste').removeClass("ere-koule");
            $('#error-artiste').hide();
        }); 
        
        
        
        $('#save-oeuvre').click(function(){
            var all_required_ok = true; 
            var no_inventaire = $('#no-inventaire').val();
            var ancien_no_inventaire = $('#ancien-no-inventaire').val();
            var categorie = $('#categorie').val();
            var titre = $('#titre').val(); 
            var signature = $('#signature').val(); 
            var titre_provisoire = $('#titre-provisoire').val(); 
            var date_creation = $('#date-creation').val();
            var statut = $('#statut').val();
            var lieu_creation = $('#lieu-creation').val();
            var longue = $('#dim-long').val(); 
            var larg = $('#dim-larg').val(); 
            var haut = $('#dim-haut').val(); 
            var materiaux = $('#materiaux').val(); 
            var technique = $('#technique').val(); 
            var description = $('#description').val();
            var legende_image = $('#legende').val(); 
            var artiste = $('#artiste-selected').val();
            var mots_cles = $('#mots-cles').val(); 
            var type_acquisition = $('#type-acquisition').val(); 
            var person_acquisition = $('#person-acquisition-selected').val();
            var date_acquisition = $('#date-acquisition').val(); 
            var prix_achat = $('#prix-achat').val();
            var valeur_assurance = $('#valeur-assurance').val(); 
            var vente = $('#vente').val(); 
            var date_vente = $('#date-vente').val(); 
            var acheteur = $('#acheteur-selected').val(); 
            var prix_vente = $('#prix-vente').val();
            var localisation = $('#localisation').val(); 
            var emplacement = $('#emplacement').val(); 
            var etat_conservation = $('#etat-conservation').val(); 
            var constat = $('#constat').val();
            var nom_restaurateur = $('#restaurateur-selected').val();
            var date_restauration = $('#date-restauration').val(); 
            var object_restauration = $('#object-restauration').val(); 
            if(no_inventaire===''){
                $('#error-no-inventaire').show();
                $('#error-no-inventaire').html('(No inventaire ne peut pas être vide !)');
                $('#no-inventaire').addClass("ere-koule");
                all_required_ok = false; 
            }
            if(titre===''){
                $('#error-titre').show();
                $('#error-titre').html('(Titre ne peut pas être vide !)');
                $('#titre').addClass("ere-koule");
                all_required_ok = false; 
            }
            if(artiste===''){
                $('#error-artiste').show();
                $('#error-artiste').html('(Nom artiste ne peut pas être vide !)');
                $('#artiste').addClass("ere-koule");
                all_required_ok = false; 
            }
            if(all_required_ok===true){
            $.get('includes/logipam/yii/web/index.php?r=site/save-oeuvre',{
                no_inventaire:no_inventaire,
                ancien_no_inventaire:ancien_no_inventaire,
                categorie:categorie,
                titre:titre,
                signature: signature,
                titre_provisoire:titre_provisoire,
                date_creation:date_creation,
                statut:statut,
                lieu_creation:lieu_creation,
                longue:longue, 
                larg:larg,
                haut:haut, 
                materiaux:materiaux,
                technique:technique,
                description:description,
                legende_image:legende_image,
                artiste:artiste,
                mots_cles:mots_cles, 
                type_acquisition:type_acquisition,
                person_acquisition:person_acquisition,
                date_acquisition:date_acquisition,
                prix_achat:prix_achat,
                valeur_assurance:valeur_assurance,
                vente:vente,
                date_vente:date_vente,
                acheteur:acheteur,
                prix_vente:prix_vente,
                localisation:localisation,
                emplacement:emplacement,
                etat_conservation:etat_conservation,
                constat:constat,
                nom_restaurateur:nom_restaurateur,
                date_restauration:date_restauration,
                object_restauration:object_restauration
                
            },function(data){
                $('#place-create-oeuvre').hide(); 
                $('#place-last-oeuvre').hide();
                $('#place-view-oeuvre').show(); 
                $('#place-view-oeuvre').html(data);
            });
        }
        });
        
        
        
        !function(source) {
            function extractor(query) {
                var result = /([^,]+)$/.exec(query);
                if(result && result[1])
                    return result[1].trim();
            return '';
        }
    
    $('#mots-cles').typeahead({
        source: source,
        updater: function(item) {
            return this.$element.val().replace(/[^,]*$/,'')+item+',';
        },
        matcher: function (item) {
          var tquery = extractor(this.query);
          if(!tquery) return false;
          return ~item.toLowerCase().indexOf(tquery.toLowerCase())
        },
        highlighter: function (item) {
          
          var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
          return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
            return '<strong>' + match + '</strong>'
          })
        }
    });
    
    }(
    <?= $js_all_keywords; ?>
    );

        
        /*    
        $('#date-creation').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        */
        
        $('#date-acquisition').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        $('#date-vente').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        $('#date-restauration').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
     });
             
    
 </script>
 