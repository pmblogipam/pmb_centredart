<?php 
    use app\models\Notices; 
?>
<div>
            <?php 
                $last_notices = Notices::findBySql("SELECT n.notice_id, n.code, n.tit1, n.typdoc, CONCAT(a.author_rejete, ' ',a.author_name) AS 'nom_auteur'  FROM notices n INNER JOIN responsability r ON (r.responsability_notice = n.notice_id) INNER JOIN authors a ON (a.author_id = r.responsability_author) WHERE n.typdoc NOT IN ('a','b') ORDER BY notice_id DESC LIMIT 10")->asArray()->all(); 
                $cat_array = ['t'=>'Peinture','o'=>'Sculpture','p'=>'Art graphique','q'=>'Objet décoratif','s'=>'Photographie',''=>'Pas de catégorie'];
               
            ?>
    <p> </p><p> </p>
    
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <th></th>    
        <th>No inventaire</th>
        <th>Titre</th>
        <th>Artiste</th>
        <th>Catégorie</th>
        </thead>
        <tbody>
            <?php 
                $k = 1;
                for($i=0;$i<count($last_notices); $i++){
                    ?>
                <tr class="notice-id" data-noticeid="<?=$last_notices[$i]['notice_id']?>">
                    <th scope="row"><?= $k; ?></th> 
                    <td><a a="href=#"><?= $last_notices[$i]['code']?></a></td>
                    <td><?= $last_notices[$i]['tit1']?></td>
                    <td><?= $last_notices[$i]['nom_auteur']?></td>
                    <td><?= $cat_array[$last_notices[$i]['typdoc']]?></td>
        
               <tr>
                <?php
                    $k++;
                    }
                ?>
           
        </table>
    
</div>

<script>
    $(document).ready(function(){
        
        $('.notice-id').click(function(){
            var notice_id = $(this).data().noticeid;
            $.get('includes/logipam/yii/web/index.php?r=site/view-oeuvre',{notice_id:notice_id},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-last-oeuvre').hide(); 
            $('#place-search-oeuvre').hide();
            $('#place-view-oeuvre').show(); 
            $('#place-view-oeuvre').html(data);
            });
        });
        
       
       
    });
</script>