
<div class="row">
    <p>&nbsp;</p>
</div>


    <div class="row">
       <div class="col-xs-4">
           <a id="add-oeuvre" class="btn btn-lg btn-primary"><i class="fa fa-plus"> Ajouter Oeuvre</i></a>
           <!-- Pour tester et developer -->
           <a id="view-oeuvre" class="btn btn-lg btn-success"><i class="fa fa-eye"> Voir les dernières oeuvres</i></a>
        </div>
        <div class="col-xs-4">
            <div class="input-group">
                    <input placeholder="Rechercher dans tous les champs" name="search" id="input-search" class="form-control input-lg" type="text">
                    <div class="input-group-btn">
                        <a id="btn-search" class="btn btn-lg btn-info">
                            Rechercher
                        </a>
                    </div>
               </div>
         </div>
    </div>
    
    <div id="place-create-oeuvre">
        
    </div>
    <div id="place-view-oeuvre">
        
    </div>
    <div id="place-last-oeuvre">
        
    </div>
    <div id="place-search-oeuvre">
    
    </div>

    <div id="place-manage-list">
        
    </div>

    <div id="place-pret-expo">

    </div>





 
 


<script>
    
$(document).ready(function(){
    $('#place-create-oeuvre').hide(); 
    $('#place-view-oeuvre').hide(); 
    $('#place-manage-list').hide();
    $('#place-pret-expo').hide();
    
    $.get('includes/logipam/yii/web/index.php?r=site/last-oeuvre',{},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-view-oeuvre').hide(); 
            $('#place-search-oeuvre').hide();
            $('#place-pret-expo').hide();
            $('#place-last-oeuvre').html(data);
        });
        
    // Manage the list 
    
    $('#list').click(function(){
        $.get('includes/logipam/yii/web/index.php?r=site/manage-list',{},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-view-oeuvre').hide();
            $('#place-last-oeuvre').hide();
            $('#place-search-oeuvre').hide();
            $('#place-pret-expo').hide();
            $('#place-manage-list').show();
            $('#place-manage-list').html(data);
        });
    });
    
   // Ajouter oeuvre  
    $('#add-oeuvre').click(function(){
        $.get('includes/logipam/yii/web/index.php?r=site/create-oeuvre',{},function(data){
            $('#place-create-oeuvre').show(); 
            $('#place-view-oeuvre').hide();
            $('#place-last-oeuvre').hide();
            $('#place-search-oeuvre').hide();
            $('#place-manage-list').hide();
            $('#place-pret-expo').hide();
            $('#place-create-oeuvre').html(data);
        });
    });
    
    $('#oeuvre').click(function(){
        $.get('includes/logipam/yii/web/index.php?r=site/last-oeuvre',{},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-view-oeuvre').hide(); 
            $('#place-search-oeuvre').hide();
            $('#place-manage-list').hide();
            $('#place-pret-expo').hide();
            $('#place-last-oeuvre').show();
            $('#place-last-oeuvre').html(data);
        });
    });
    
    $('#view-oeuvre').click(function(){
        $.get('includes/logipam/yii/web/index.php?r=site/last-oeuvre',{},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-view-oeuvre').hide(); 
            $('#place-search-oeuvre').hide();
            $('#place-manage-list').hide();
            $('#place-pret-expo').hide();
            $('#place-last-oeuvre').show();
            $('#place-last-oeuvre').html(data);
        });
    });
    
    $('#btn-search').click(function(){
       
        var search = $('#input-search').val();
        
        $.get('includes/logipam/yii/web/index.php?r=site/search-oeuvre',{search:search},function(data){
            $('#place-create-oeuvre').hide(); 
            $('#place-view-oeuvre').hide(); 
            $('#place-last-oeuvre').hide();
            $('#place-manage-list').hide();
            $('#place-pret-expo').hide();
            $('#place-search-oeuvre').show();
            $('#place-search-oeuvre').html(data);
        });
    });
    
    
    $("#searchPlace").hide(); 
    $("#addOeuvre").click(function(){
        $("#notice").show(); 
        $("#searchPlace").hide();
    });
    $('#date-creation').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        $("#search").click(function(){
            $("#notice").hide(); 
            $("#searchPlace").show(); 
            $.get('includes/logipam/yii/web/index.php?r=site/get-search',{},function(data){
            
            $('#searchPlace').html(data);
            });
        });
        
     $('#pret').click(function(){
         $('#place-create-oeuvre').hide(); 
            $('#place-view-oeuvre').hide(); 
            $('#place-last-oeuvre').hide();
            $('#place-manage-list').hide();
            $('#place-search-oeuvre').hide();
            $('#place-pret-expo').show();
            $.get('includes/logipam/yii/web/index.php?r=site/pret-expo',{},function(data){
                $('#place-pret-expo').html(data);
            });
     });   
        
});    

</script>