
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' lang='fr' charset='utf-8'>
  <head>
    <title>
      PMB
    </title>
	<meta name='author' content='PMB Group' />
	<meta name='description' content='Logiciel libre de gestion de m�diath�que' />
	<meta name='keywords' content='logiciel, gestion, biblioth�que, m�diath�que, libre, free, software, mysql, php, linux, windows, mac' />
	<!--<meta http-equiv='Pragma' content='no-cache' />
	<meta http-equiv='Cache-Control' content='no-cache' />-->
 	<meta http-equiv='Content-Type' content="text/html; charset=utf-8" />
	<meta http-equiv='Content-Language' content='fr_FR' />
	<link rel='stylesheet' type='text/css' href='./styles/common/common.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/common/dashboard.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/common/font-awesome.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/dgrowl-pure.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/dijit-custom.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/gestion-patch.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/open-sans.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/pure.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-acquisition-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-admin-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-autorites-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-cata-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-circ-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-cms-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-demandes-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-dsi-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-edit-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-fichier-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/set-generique-color.css' title='lefttoright' />
	<link rel='stylesheet' type='text/css' href='./styles/pure/uikit.css' title='lefttoright' />
	<script type='text/javascript' src='./styles/pure/javascript/ajquery-3.1.1.min.js?1488490114' ></script>
	<script type='text/javascript' src='./styles/pure/javascript/dom-adjust.js?1512076304' ></script>
	<script type='text/javascript' src='./styles/pure/javascript/k-set-wyrkit.js?1514489256' ></script>
	<script type='text/javascript' src='./styles/pure/javascript/uikit.js?1510929704' ></script>
	<script type='text/javascript' src='./styles/pure/javascript/wyrkit.js?1488490114' ></script>
	<script type='text/javascript' src='./styles/pure/javascript/zuikit-icons-add-pmb.js?1491942844' ></script>
	<script type='text/javascript' src='./styles/pure/javascript/zuikit-icons.js?1510929704' ></script>
	<style type='text/css'>
	
		</style>
	<link rel="SHORTCUT ICON" href="images/favicon.ico" />
	<script src="./javascript/popup.js" type="text/javascript"></script>
	<script src="./javascript/drag_n_drop.js" type="text/javascript"></script>
	<script src="./javascript/handle_drop.js" type="text/javascript"></script>
	<script src="./javascript/element_drop.js" type="text/javascript"></script>
	<script src="./javascript/cart_div.js" type="text/javascript"></script>
	<script src="./javascript/misc.js" type="text/javascript"></script>
	<script src="./javascript/http_request.js" type="text/javascript"></script>
	<script type="text/javascript"> var base_path='.' </script>
	<script src='./javascript/tablist.js' type="text/javascript"></script>
	<script src='./javascript/sorttable.js' type='text/javascript'></script>
	<script src='./javascript/templates.js' type='text/javascript'></script>
	<script type="text/javascript">
		function keep_context(myObject,methodName){
			return function(){
				return myObject[methodName]();
			}
		}
		// Fonction a utilisier pour l'encodage des URLs en javascript
		function encode_URL(data){
			var docCharSet = document.characterSet ? document.characterSet : document.charset;
			if(docCharSet == "UTF-8"){
				return encodeURIComponent(data);
			}else{
				return escape(data);
			}
		}
	</script>

		<link rel='stylesheet' type='text/css' href='./javascript/dojo/dijit/themes/claro/claro.css' />
		<script type='text/javascript'>
			var dojoConfig = {
				parseOnLoad: true,
				locale: 'fr-fr',
				isDebug: false,
				usePlainJson: true,
				packages: [{
					name: 'pmbBase',
					location:'../../..'
				},{
					name: 'd3',
					location:'../../d3'
				}],
				deps: ['apps/pmb/MessagesStore', 'apps/pmb/AceManager', 'dgrowl/dGrowl', 'dojo/ready'],
				callback:function(MessagesStore, AceManager, dGrowl, ready){
					window.pmbDojo = {};
					pmbDojo.uploadMaxFileSize = 32768,
					pmbDojo.messages = new MessagesStore({url:'./ajax.php?module=ajax&categ=messages', directInit:false});
					pmbDojo.aceManager = new AceManager();
					ready(function(){
						new dGrowl({'channels':[{'name':'info','pos':2},{'name':'error', 'pos':3},{'name':'service','pos':1}]});
					});
				},
	        };
		</script>
		<script type='text/javascript' src='./javascript/dojo/dojo/dojo.js'></script><link href='./javascript/dojo/dojox/editor/plugins/resources/editorPlugins.css' type='text/css' rel='stylesheet' />
		<link href='./javascript/dojo/dojox/editor/plugins/resources/css/InsertEntity.css' type='text/css' rel='stylesheet' />
		<link href='./javascript/dojo/dojox/editor/plugins/resources/css/PasteFromWord.css' type='text/css' rel='stylesheet' />
		<link href='./javascript/dojo/dojox/editor/plugins/resources/css/InsertAnchor.css' type='text/css' rel='stylesheet' />
		<link href='./javascript/dojo/dojox/editor/plugins/resources/css/LocalImage.css' type='text/css' rel='stylesheet' />
		<link href='./javascript/dojo/dojox/form/resources/FileUploader.css' type='text/css' rel='stylesheet' />
		<link href='./javascript/dojo/dgrowl/dGrowl.css' type='text/css' rel='stylesheet' />
		<script type='text/javascript'>
			dojo.require('dijit.Editor');
			dojo.require('dijit._editor.plugins.LinkDialog');
			dojo.require('dijit._editor.plugins.FontChoice');
			dojo.require('dijit._editor.plugins.TextColor');
			dojo.require('dijit._editor.plugins.FullScreen');
			dojo.require('dijit._editor.plugins.ViewSource');
			dojo.require('dojox.editor.plugins.InsertEntity');
			dojo.require('dojox.editor.plugins.TablePlugins');
			dojo.require('dojox.editor.plugins.ResizeTableColumn');
			dojo.require('dojox.editor.plugins.PasteFromWord');
			dojo.require('dojox.editor.plugins.InsertAnchor');
			dojo.require('dojox.editor.plugins.Blockquote');
			dojo.require('dojox.editor.plugins.LocalImage');
		</script>
	<script type="text/javascript">var trueids="0";</script>

		<script type='text/javascript' src='./javascript/pmbtoolkit.js'></script>	
		<script type='text/javascript' src='./javascript/notification.js'></script>	</head><body class='select claro' id='body_current_module' page_name='select'><script type='text/javascript'>document.title="Selection";window.status="Selection";</script>
<script type='text/javascript' src='./javascript/select.js'></script>
<script type='text/javascript'>
<!--
// affichage des raccourcis

function clean_raccourci() {
	setTimeout("top.document.getElementById('keystatus').firstChild.nodeValue=' '",1000);
}

function touche(e) {
	if (!e) var e = window.event;
	if (e.keyCode) key = e.keyCode;
		else if (e.which) key = e.which;
		window.clearTimeout(timer);
		kill_frame('frame_shortcuts');
	
	top.document.getElementById('keystatus').firstChild.nodeValue='Esc - '+String.fromCharCode(key);
	top.document.getElementById('keystatus').style.color='#FF0000';
	key = String.fromCharCode(key);
	key = key.toLowerCase();
	key = key.charCodeAt(0);

	//Traitement des actions
	switch(key) {
		//case 115:
		//	if (document.getElementById('btsubmit')) document.getElementById('btsubmit').focus();
		//	e.cancelBubble = true;
		//	if (e.stopPropagation) { e.stopPropagation(); }
		//	clean_raccourci();
		//	break;
		default:	
			switch(key) {
				default : clean_raccourci(); break;
			}
	}
	document.onkeydown=backhome;
}

function backhome(e){
	if (!e) var e = window.event;
	if (e.keyCode) key = e.keyCode;
		else if (e.which) key = e.which;

	if(key == 27) {
		propagate=true;
		//R�cup�ration de l'objet d'origine
		if (e.target) origine=e.target; else origine=e.srcElement;
	    if (origine.getAttribute('completion')) {
			id=origine.getAttribute('id');
			if (document.getElementById('d'+id).style.display=='block') {
				propagate=false;
			}
		}		
		if (propagate) {
			timer=setTimeout('ShowShortcuts()',2000);
			top.document.getElementById('keystatus').firstChild.nodeValue='Esc';
			top.document.getElementById('keystatus').style.color='#FF0000';
			window.focus();
			document.onkeydown=touche;
		}
	}	
}

document.onkeydown=backhome;

function ShowShortcuts(){
	frame_shortcuts('./includes/shortcuts/frame_shortcuts.php')
}

//-->
</script>

<script type='text/javascript'>
	self.focus();
	</script><script type='text/javascript'>
		function replace_texte(string,text,by) {
		    var strLength = string.length, txtLength = text.length;
		    if ((strLength == 0) || (txtLength == 0)) return string;
		
		    var i = string.indexOf(text);
		    if ((!i) && (text != string.substring(0,txtLength))) return string;
		    if (i == -1) return string;
		
		    var newstr = string.substring(0,i) + by;
		
		    if (i+txtLength < strLength)
		        newstr += replace_texte(string.substring(i+txtLength,strLength),text,by);
		
		    return newstr;
		}
		
		function reverse_html_entities(text) {
		    
		    text = replace_texte(text,'&quot;',unescape('%22'));
		    text = replace_texte(text,'&amp;',unescape('%26'));
		    text = replace_texte(text,'&lt;',unescape('%3C'));
		    text = replace_texte(text,'&gt;',unescape('%3E'));
		    text = replace_texte(text,'&nbsp;',unescape('%A0'));
		    text = replace_texte(text,'&iexcl;',unescape('%A1'));
		    text = replace_texte(text,'&cent;',unescape('%A2'));
		    text = replace_texte(text,'&pound;',unescape('%A3'));
		    text = replace_texte(text,'&yen;',unescape('%A5'));
		    text = replace_texte(text,'&brvbar;',unescape('%A6'));
		    text = replace_texte(text,'&sect;',unescape('%A7'));
		    text = replace_texte(text,'&uml;',unescape('%A8'));
		    text = replace_texte(text,'&copy;',unescape('%A9'));
		    text = replace_texte(text,'&ordf;',unescape('%AA'));
		    text = replace_texte(text,'&laquo;',unescape('%AB'));
		    text = replace_texte(text,'&not;',unescape('%AC'));
		    text = replace_texte(text,'&shy;',unescape('%AD'));
		    text = replace_texte(text,'&reg;',unescape('%AE'));
		    text = replace_texte(text,'&macr;',unescape('%AF'));
		    text = replace_texte(text,'&deg;',unescape('%B0'));
		    text = replace_texte(text,'&plusmn;',unescape('%B1'));
		    text = replace_texte(text,'&sup2;',unescape('%B2'));
		    text = replace_texte(text,'&sup3;',unescape('%B3'));
		    text = replace_texte(text,'&acute;',unescape('%B4'));
		    text = replace_texte(text,'&micro;',unescape('%B5'));
		    text = replace_texte(text,'&para;',unescape('%B6'));
		    text = replace_texte(text,'&middot;',unescape('%B7'));
		    text = replace_texte(text,'&cedil;',unescape('%B8'));
		    text = replace_texte(text,'&sup1;',unescape('%B9'));
		    text = replace_texte(text,'&ordm;',unescape('%BA'));
		    text = replace_texte(text,'&raquo;',unescape('%BB'));
		    text = replace_texte(text,'&frac14;',unescape('%BC'));
		    text = replace_texte(text,'&frac12;',unescape('%BD'));
		    text = replace_texte(text,'&frac34;',unescape('%BE'));
		    text = replace_texte(text,'&iquest;',unescape('%BF'));
		    text = replace_texte(text,'&Agrave;',unescape('%C0'));
		    text = replace_texte(text,'&Aacute;',unescape('%C1'));
		    text = replace_texte(text,'&Acirc;',unescape('%C2'));
		    text = replace_texte(text,'&Atilde;',unescape('%C3'));
		    text = replace_texte(text,'&Auml;',unescape('%C4'));
		    text = replace_texte(text,'&Aring;',unescape('%C5'));
		    text = replace_texte(text,'&AElig;',unescape('%C6'));
		    text = replace_texte(text,'&Ccedil;',unescape('%C7'));
		    text = replace_texte(text,'&Egrave;',unescape('%C8'));
		    text = replace_texte(text,'&Eacute;',unescape('%C9'));
		    text = replace_texte(text,'&Ecirc;',unescape('%CA'));
		    text = replace_texte(text,'&Euml;',unescape('%CB'));
		    text = replace_texte(text,'&Igrave;',unescape('%CC'));
		    text = replace_texte(text,'&Iacute;',unescape('%CD'));
		    text = replace_texte(text,'&Icirc;',unescape('%CE'));
		    text = replace_texte(text,'&Iuml;',unescape('%CF'));
		    text = replace_texte(text,'&ETH;',unescape('%D0'));
		    text = replace_texte(text,'&Ntilde;',unescape('%D1'));
		    text = replace_texte(text,'&Ograve;',unescape('%D2'));
		    text = replace_texte(text,'&Oacute;',unescape('%D3'));
		    text = replace_texte(text,'&Ocirc;',unescape('%D4'));
		    text = replace_texte(text,'&Otilde;',unescape('%D5'));
		    text = replace_texte(text,'&Ouml;',unescape('%D6'));
		    text = replace_texte(text,'&times;',unescape('%D7'));
		    text = replace_texte(text,'&Oslash;',unescape('%D8'));
		    text = replace_texte(text,'&Ugrave;',unescape('%D9'));
		    text = replace_texte(text,'&Uacute;',unescape('%DA'));
		    text = replace_texte(text,'&Ucirc;',unescape('%DB'));
		    text = replace_texte(text,'&Uuml;',unescape('%DC'));
		    text = replace_texte(text,'&Yacute;',unescape('%DD'));
		    text = replace_texte(text,'&THORN;',unescape('%DE'));
		    text = replace_texte(text,'&szlig;',unescape('%DF'));
		    text = replace_texte(text,'&agrave;',unescape('%E0'));
		    text = replace_texte(text,'&aacute;',unescape('%E1'));
		    text = replace_texte(text,'&acirc;',unescape('%E2'));
		    text = replace_texte(text,'&atilde;',unescape('%E3'));
		    text = replace_texte(text,'&auml;',unescape('%E4'));
		    text = replace_texte(text,'&aring;',unescape('%E5'));
		    text = replace_texte(text,'&aelig;',unescape('%E6'));
		    text = replace_texte(text,'&ccedil;',unescape('%E7'));
		    text = replace_texte(text,'&egrave;',unescape('%E8'));
		    text = replace_texte(text,'&eacute;',unescape('%E9'));
		    text = replace_texte(text,'&ecirc;',unescape('%EA'));
		    text = replace_texte(text,'&euml;',unescape('%EB'));
		    text = replace_texte(text,'&igrave;',unescape('%EC'));
		    text = replace_texte(text,'&iacute;',unescape('%ED'));
		    text = replace_texte(text,'&icirc;',unescape('%EE'));
		    text = replace_texte(text,'&iuml;',unescape('%EF'));
		    text = replace_texte(text,'&eth;',unescape('%F0'));
		    text = replace_texte(text,'&ntilde;',unescape('%F1'));
		    text = replace_texte(text,'&ograve;',unescape('%F2'));
		    text = replace_texte(text,'&oacute;',unescape('%F3'));
		    text = replace_texte(text,'&ocirc;',unescape('%F4'));
		    text = replace_texte(text,'&otilde;',unescape('%F5'));
		    text = replace_texte(text,'&ouml;',unescape('%F6'));
		    text = replace_texte(text,'&divide;',unescape('%F7'));
		    text = replace_texte(text,'&oslash;',unescape('%F8'));
		    text = replace_texte(text,'&ugrave;',unescape('%F9'));
		    text = replace_texte(text,'&uacute;',unescape('%FA'));
		    text = replace_texte(text,'&ucirc;',unescape('%FB'));
		    text = replace_texte(text,'&uuml;',unescape('%FC'));
		    text = replace_texte(text,'&yacute;',unescape('%FD'));
		    text = replace_texte(text,'&thorn;',unescape('%FE'));
		    text = replace_texte(text,'&yuml;',unescape('%FF'));
		    return text;
		
		}
		</script>
			<div id='att' style='z-Index:1000'></div>		
			<script src='./javascript/ajax.js'></script>
			<div class='row'>
				<label for='selector_title' class='etiquette'>S&eacute;lection d&#039;un auteur</label>
				</div>
			<div class='row'>
			
			<form name='search_form' method='post' action='./select.php?what=auteur&caller=notice&param1=f_aut0_id1&param2=f_aut01'>
				<select id='type_autorite' name='type_autorite'>
					<option value='7' selected>Afficher tout</option>
					<option value='70' >Personne physique</option>
					<option value='71' >Collectivité</option>
					<option value='72'  >Congrès</option>
				</select>
				<input type='text' name='f_user_input' value="P. HECTOR, E. ">
				&nbsp;
				<input type='submit' class='bouton_small' value='Rechercher' />
				<input type='button' class='bouton_small' onclick="document.location='./select.php?what=auteur&caller=notice&param1=f_aut0_id1&param2=f_aut01&action=add&deb_rech='+this.form.f_user_input.value+'&no_display=0&type_autorite='" value='Cr&eacute;er un auteur' />
			</form>
			<script type='text/javascript'>
				<!--
				document.forms['search_form'].elements['f_user_input'].focus();
				-->
			</script>
		
	<script type='text/javascript'>
	
	function set_parent(f_caller, id_value, libelle_value, callback){
		var w = window;
		var p1 = 'f_aut0_id1';
		var p2 = 'f_aut01';
		//on enl�ve le dernier _X
		var tmp_p1 = p1.split('_');
		var tmp_p1_length = tmp_p1.length;
		tmp_p1.pop();
		var p1bis = tmp_p1.join('_');
		
		var tmp_p2 = p2.split('_');
		var tmp_p2_length = tmp_p2.length;
		tmp_p2.pop();
		var p2bis = tmp_p2.join('_');
		
		var max_aut = w.opener.document.getElementById(p1bis.replace('id','max_aut'));
		if(max_aut && (p1bis.replace('id','max_aut').substr(-7)=='max_aut')){
			var trouve=false;
			var trouve_id=false;
			for(i_aut=0;i_aut<=max_aut.value;i_aut++){
				if(w.opener.document.getElementById(p1bis+'_'+i_aut).value==0){
					w.opener.document.getElementById(p1bis+'_'+i_aut).value=id_value;
					w.opener.document.getElementById(p2bis+'_'+i_aut).value=reverse_html_entities(libelle_value);
					trouve=true;
					break;
				}else if(w.opener.document.getElementById(p1bis+'_'+i_aut).value==id_value){
					trouve_id=true;
				}
			}
			if(!trouve && !trouve_id){
				w.opener.add_line(p1bis.replace('_id',''));
				w.opener.document.getElementById(p1bis+'_'+i_aut).value=id_value;
				w.opener.document.getElementById(p2bis+'_'+i_aut).value=reverse_html_entities(libelle_value);
			}
			if(callback)
				w.opener[callback](p1bis.replace('_id','')+'_'+i_aut);
		}else{
			w.opener.document.forms[f_caller].elements['f_aut0_id1'].value = id_value;
			w.opener.document.forms[f_caller].elements['f_aut01'].value = reverse_html_entities(libelle_value);
			if(callback)
				w.opener[callback]('');
			w.close();
		}
	}

	</script>
<div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut612'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut612'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut612' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '865', 'P. HECTOR, E. ','')">P. HECTOR, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut154'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut154'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut154' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '218', 'JOB, E.P.H ','')">JOB, E.P.H </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut797'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut797'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut797' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '1060', 'P., J. ','')">P., J. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut581'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut581'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut581' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '830', 'P. ABELLARD, G. ','')">P. ABELLARD, G. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut577'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut577'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut577' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '824', 'GALIOTH, E. ','')">GALIOTH, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut570'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut570'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut570' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '816', 'LUBIN, E. ','')">LUBIN, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut568'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut568'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut568' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '814', 'EDNER, P. ','')">EDNER, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut509'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut509'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut509' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '745', 'PIERRE, E. ','')">PIERRE, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut503'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut503'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut503' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '739', 'R. BRESIER, P. ','')">R. BRESIER, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut502'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut502'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut502' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '737', 'P. MICHEL, S. ','')">P. MICHEL, S. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut500'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut500'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut500' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '735', 'A. NICOLAS, P. ','')">A. NICOLAS, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut494'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut494'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut494' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '729', 'J. CADET, P. ','')">J. CADET, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut483'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut483'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut483' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '715', 'JOCELYN, E. ','')">JOCELYN, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut587'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut587'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut587' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '836', 'MAXIME, E. ','')">MAXIME, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut597'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut597'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut597' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '847', 'K. COLAS, E. ','')">K. COLAS, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut757'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut757'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut757' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '1018', 'WAH , E. ','')">WAH , E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut742'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut742'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut742' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '1002', 'Fr&egrave;re, E. ','')">Frère, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut741'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut741'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut741' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '1001', 'CANTAVE, P.A. ','')">CANTAVE, P.A. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut718'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut718'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut718' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '978', 'JEAN BAPTISTE, E. ','')">JEAN BAPTISTE, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut704'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut704'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut704' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '965', 'PHILTON, E. ','')">PHILTON, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut703'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut703'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut703' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '964', 'MENOS, E. ','')">MENOS, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut691'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut691'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut691' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '950', 'P. ETIENNE, Georges ','')">P. ETIENNE, Georges </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut673'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut673'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut673' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '932', 'Paul HECTOR, Georges ','')">Paul HECTOR, Georges </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut670'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut670'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut670' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '929', 'A. CANTAVE, P. ','')">A. CANTAVE, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut649'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut649'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut649' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '906', 'J. RIGAUD, E. ','')">J. RIGAUD, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut475'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut475'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut475' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '708', 'FREMONT, E. ','')">FREMONT, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut459'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut459'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut459' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '695', 'HECTOR, R. ','')">HECTOR, R. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut276'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut276'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut276' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '129', 'BOURSIQUOT, P. ','')">BOURSIQUOT, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut258'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut258'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut258' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '260', 'ORELUS, E. ','')">ORELUS, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut242'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut242'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut242' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '130', 'BRESIER , P.R. ','')">BRESIER , P.R. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut206'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut206'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut206' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '131', 'BRIERE , E. ','')">BRIERE , E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut175'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut175'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut175' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '138', 'CAMILIEN , P. ','')">CAMILIEN , P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut157'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut157'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut157' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '229', 'LATAILLADE, E. ','')">LATAILLADE, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut148'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut148'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut148' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '277', 'PIERRE , P. ','')">PIERRE , P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut134'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut134'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut134' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '214', 'JEAN-BAPTISTE, E. ','')">JEAN-BAPTISTE, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut102'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut102'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut102' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '292', 'SAINTUS , J.E. ','')">SAINTUS , J.E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut34'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut34'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut34' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '517', 'PHILDOR, P. ','')">PHILDOR, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut292'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut292'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut292' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '343', 'MORANTUS, E. ','')">MORANTUS, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut298'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut298'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut298' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '351', 'BEAUBRUN, P.R ','')">BEAUBRUN, P.R </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut303'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut303'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut303' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '360', 'JOSEPH, E. ','')">JOSEPH, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut433'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut433'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut433' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '639', 'GUERIN, E. ','')">GUERIN, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut385'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut385'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut385' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '576', 'E. BRIERRE, Mario ','')">E. BRIERRE, Mario </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut371'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut371'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut371' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '557', 'J. CHARLES, E. ','')">J. CHARLES, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut354'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut354'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut354' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '533', 'JACQUES, E. ','')">JACQUES, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut347'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut347'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut347' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '513', 'ABELARD, E. ','')">ABELARD, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut346'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut346'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut346' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '508', 'JEAN CHARLES, E. ','')">JEAN CHARLES, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut341'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut341'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut341' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '499', 'HUGUES, P. ','')">HUGUES, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut334'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut334'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut334' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '450', 'PIERRETTE, E. ','')">PIERRETTE, E. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut324'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut324'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut324' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '423', 'J.E. SAINTUS, Mme ','')">J.E. SAINTUS, Mme </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut322'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut322'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut322' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '416', 'GREICH, P. ','')">GREICH, P. </a></div><div class='row'><span><a href=# onmouseover="z=document.getElementById('zoom_statut19'); z.style.display=''; " onmouseout="z=document.getElementById('zoom_statut19'); z.style.display='none'; "><img src='./images/spacer.gif' class='statutnot1' style='width:7px; height:7px; vertical-align:middle; margin-left:7px' /></a></span>
			<div id='zoom_statut19' style='border: solid 2px #555555; background-color: #FFFFFF; position: absolute; display:none; z-index: 2000;'><font color='black'><b>Statut par d&eacute;faut</b></font></div><a href='#' onclick="set_parent('notice', '754', 'ABELLARD, E. ','')">ABELLARD, E. </a></div><div class='row'>&nbsp;<hr /></div><div align='center'><div align='center'><strong>1</strong> (1 - 51 / 51)<span style='float:right;'> Par page :  <a id='derniere' href='./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&rech_regexp=&user_input=P.%20HECTOR%2C%20E.%20&type_autorite=0&page=1&nbr_lignes=51&nb_per_page_custom=25' >25</a>  <a id='derniere' href='./select.php?what=auteur&caller=notice&param1=f_aut0_id&param2=f_aut0&rech_regexp=&user_input=P.%20HECTOR%2C%20E.%20&type_autorite=0&page=1&nbr_lignes=51&nb_per_page_custom=50' >50</a> </span></div></div></div>