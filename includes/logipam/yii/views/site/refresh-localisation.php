<table class="table table-striped" id="tablo-localisation">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $m = 1;
                    foreach($localisation as $lo){
                        ?>
                <tr>
                    <td><?= $m; ?></td>
                    <td class="modifye" data-type="text" data-name="localisation" data-pk="<?= $lo['id']; ?>" data-url="includes/logipam/yii/web/index.php?r=site/update-list">
                        <?= $lo['nom_localisation']; ?>
                    </td>
                    <td><span class="fa fa-trash efase-localisation" data-idlocalisation="<?= $lo['id']; ?>"></span></td>
                </tr>
                <?php 
                    
                    $m++;
                    }
                ?>
            </tbody>
                
</table>

<script>
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'Aucune information',
          
            
        });
    });
</script>