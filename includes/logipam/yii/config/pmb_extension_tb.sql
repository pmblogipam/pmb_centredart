/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Jan 2, 2018
 */

CREATE TABLE `logipam_presence` ( `id` INT NOT NULL AUTO_INCREMENT , `id_empr` INT NOT NULL , `empr_cb` VARCHAR(32) NOT NULL , `date_presence` DATE NOT NULL , `time_presence` TIME NOT NULL , `day_presence` INT NOT NULL , PRIMARY KEY (`id`), INDEX (`id_empr`), INDEX (`empr_cb`), INDEX (`day_presence`)) ENGINE = InnoDB;

