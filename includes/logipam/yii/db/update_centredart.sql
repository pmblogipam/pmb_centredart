/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Jul 30, 2018
 */

-- Table pour enregistrer la liste des mots cles 
CREATE TABLE `notices_keywords` ( `id` INT NOT NULL AUTO_INCREMENT , `mots_cles` VARCHAR(255) NOT NULL , `creer_par` VARCHAR(64) NULL , `date_creation` DATETIME NULL , `modifie_par` VARCHAR(64) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`), UNIQUE (`mots_cles`)) ENGINE = InnoDB;

-- Table pour enregistrer les autres types de personnes en interaction avec les oeuves 
CREATE TABLE `authors_oeuvre` ( `id` INT NOT NULL AUTO_INCREMENT , `prenom` VARCHAR(255) NULL , `nom` VARCHAR(255) NULL , `date_person` VARCHAR(64) NULL , `index_person` TEXT NULL , `type` INT NULL COMMENT '1: Personne lie a l acquisition; 2: Acheteur; 3:Nom restaurateur' , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifie_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`prenom`), INDEX (`nom`)) ENGINE = InnoDB;

-- Table enregistrement mots cles des notices 
CREATE TABLE `oeuvre_motcles` ( `id` INT NOT NULL AUTO_INCREMENT , `notice_id` INT NOT NULL , `mots_cles` TEXT NULL , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifie_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table enrgistrement des infos de provenance de l'oeuvre 
CREATE TABLE `oeuvre_provenance` ( `id` INT NOT NULL AUTO_INCREMENT , `notice_id` INT NOT NULL , `type_acquisition` VARCHAR(255) NULL , `person_acquisition` INT NULL , `date_acquisition` DATE NULL , `prix_achat` FLOAT NULL , `valeur_assurance` FLOAT NULL , `vente` TEXT NULL , `date_vente` DATETIME NULL , `acheteur` INT NULL , `prix_vente` FLOAT NULL , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifier_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table enregistrement des informations physique sur l'oeuvre 
CREATE TABLE `oeuvre_physique` ( `id` INT NOT NULL AUTO_INCREMENT , `notice_id` INT NOT NULL , `localisation` VARCHAR(255) NULL , `emplacement` VARCHAR(255) NULL , `etat_conservation` VARCHAR(255) NULL , `constat` TEXT NULL , `nom_restaurateur` INT NULL , `date_restauration` DATETIME NULL , `object_restauration` TEXT NULL , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifier_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`notice_id`)) ENGINE = InnoDB;

-- Table enregistrement d'autres informations de l'oeuvre 
CREATE TABLE `notices_other_data` ( `id` INT NOT NULL AUTO_INCREMENT , `notices_id` INT NOT NULL , `lieu_creation` VARCHAR(255) NULL , `longue` FLOAT NULL , `larg` FLOAT NULL , `haut` FLOAT NULL , `materiaux` VARCHAR(255) NULL , `technique` VARCHAR(255) NULL , `description` TEXT NULL , `legende_image` VARCHAR(255) NULL , `creer_par` VARCHAR(64) NULL , `date_creation` DATETIME NULL , `modifier_par` VARCHAR(64) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; 

-- Table enregistrement des localisations des oeuvres
CREATE TABLE `oeuvre_localisation` ( `id` INT NOT NULL AUTO_INCREMENT , `nom_localisation` VARCHAR(255) NOT NULL , `cree_par` VARCHAR(255) NULL , `modifie_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; 

-- Modifier la table notices_custom_values 
ALTER TABLE `notices_custom_values` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);

-- 09/10/2018
-- Table pour enregistrer les musees 
CREATE TABLE `collection_musee` ( `id` INT NOT NULL AUTO_INCREMENT , `nom_musee` VARCHAR(255) NOT NULL , `adresse_musee` VARCHAR(255) NULL , `phone_musee` VARCHAR(32) NULL , `date_creation` DATETIME NULL , `cree_par` VARCHAR(64) NULL , `date_modification` DATETIME NULL , `modifier_par` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`nom_musee`)) ENGINE = InnoDB;

-- Tbale pour enregistrer les emprunteurs des oeuvres 
CREATE TABLE `collection_emprunteur` ( `id` INT NOT NULL AUTO_INCREMENT , `prenom_empr` VARCHAR(255) NOT NULL , `nom_empr` VARCHAR(255) NOT NULL , `phone_empr` VARCHAR(64) NULL , `adresse_empr` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `cree_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , `modifie_par` VARCHAR(255) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;